﻿package
{
	import flash.display.*;
	import flash.events.*;
	import flash.utils.*;
	import net.wg.gui.battle.random.views.*;
	import net.wg.infrastructure.base.*;
	import polarfoxInjector.AbstractViewInjector;
	import polarfoxInjector.IAbstractInjector;
	import battle.ModBattleObserverUI;
	
	public class InjectFlash extends AbstractViewInjector implements IAbstractInjector
	{
		[Embed(source = 'font/BattleObserver.ttf', fontFamily = 'BattleObserver', fontName = 'BattleObserver', mimeType = 'application/x-font', fontStyle = 'normal', fontWeight = 'normal', embedAsCFF = 'false')]
		private var BattleObserver:Class;
		public function InjectFlash()
		{
			super();
		}
		
		override public function get componentUI():Class
		{
			return ModBattleObserverUI;
		}
		
		override public function get componentName():String
		{
			return "ModBattleObserverUI";
		}
	}
}