package polarfoxInjector
{
	import flash.events.*;
	import flash.display.DisplayObject;
	import net.wg.gui.battle.views.BaseBattlePage;
	import net.wg.gui.components.containers.MainViewContainer;
	import net.wg.gui.components.containers.ManagedContainer;
	import net.wg.infrastructure.base.AbstractView;
	import net.wg.infrastructure.interfaces.IManagedContainer;
	import net.wg.infrastructure.interfaces.IView;
	import net.wg.infrastructure.managers.impl.ContainerManagerBase;
	import net.wg.data.constants.ContainerTypes;
	
	public class AbstractViewInjector extends AbstractView implements IAbstractInjector
	{
		
		public function AbstractViewInjector()
		{
			super();
		}
		
		override protected function onPopulate():void
		{
			var container:IManagedContainer = null;
			var viewContainer:IManagedContainer = null;
			var componentContainer:IManagedContainer = null;
			var BattleView:IView = null;
			super.onPopulate();
			for each (componentContainer in(App.containerMgr as ContainerManagerBase).containersMap)
			{
				if (componentContainer as MainViewContainer != null)
				{
					container = componentContainer;
				}
				if ((componentContainer as ManagedContainer).type == ContainerTypes.WINDOW)
				{
					viewContainer = componentContainer;
				}
			}
			for (var idx:int = 0; idx < container.numChildren; idx++)
			{
				BattleView = container.getChildAt(idx) as IView;
				if (BattleView != null && BattleView as AbstractView is BaseBattlePage)
				{
					this.registerComponent(BattleView);
					break;
				}
			}
			container.setFocusedView(container.getTopmostView());
			viewContainer.removeChild(this);
		}
		
		private function registerComponent(battlePage:IView):void
		{
			var BattlePageUI:AbstractView = battlePage as AbstractView;
			var componentRef:Class = new this.componentUI(BattlePageUI, this.componentName);
			BattlePageUI.addChildAt(componentRef as DisplayObject, 1);
			BattlePageUI.registerFlashComponent(componentRef, this.componentName);
		}
		
		public function get componentUI():Class  { return null; }
		
		public function get componentName():String  { return null; }
	}
}