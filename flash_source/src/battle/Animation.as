package battle
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	public class Animation
	{
		public static var barIndex:Object = new Object();
		
		public static function onDispose():void
		{
			barIndex = null;
		}
		
		private static function animateBar(event:Event):void
		{
			var name:String = event.target.name;
			if (event.target.scaleX > barIndex[name])
			{
				event.target.scaleX -= name == 'greenBar' || name == 'redBar' ? 0.002 : 0.01;
			}
			else
			{
				event.target.removeEventListener(Event.ENTER_FRAME, animateBar);
				event.target.scaleX = barIndex[name];
				//delete barIndex[name];
			}
		}
		
		private static function animateBase(event:Event):void
		{
			if (event.target.scaleX < barIndex[event.target.name])
			{
				event.target.scaleX += 0.002;
			}
			else
			{
				event.target.removeEventListener(Event.ENTER_FRAME, animateBase);
				event.target.scaleX = barIndex[event.target.name];
				//delete barIndex[event.target.name];
			}
		}
		
		public static function runBaseAnimation(obj:DisplayObject, scale:Number):void
		{
			if (obj.hasEventListener(Event.ENTER_FRAME))
			{
				obj.removeEventListener(Event.ENTER_FRAME, animateBase);
			}
			barIndex[obj.name] = scale;
			obj.addEventListener(Event.ENTER_FRAME, animateBase, false, 0, true);
		}
		
		public static function runAnimation(obj:DisplayObject, scale:Number):void
		{
			if (obj.hasEventListener(Event.ENTER_FRAME))
			{
				obj.removeEventListener(Event.ENTER_FRAME, animateBar);
			}
			barIndex[obj.name] = scale;
			obj.addEventListener(Event.ENTER_FRAME, animateBar, false, 0, true);
		}
	}
}