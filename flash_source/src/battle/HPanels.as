package battle
{
	import flash.display.*;
	import flash.filters.*;
	import flash.text.*;
	import flash.geom.ColorTransform;
	import battle.Animation;
	
	public class HPanels
	{
		private static var panels:Object = new Object();
		private static var listItems:Object = new Object();
		private static var logsformat:TextFormat = new TextFormat("$TitleFont", 15, 0xFAFAFA);
		private static var glow:GlowFilter = new GlowFilter(0, 0.9, 2, 2, 1.9, BitmapFilterQuality.LOW, false, false);
		private static var _battlePage:* = null;
		
		public function HPanels()
		{
			super();
		}
		
		public static function setBattlePage(battlePage:*):void
		{
			_battlePage = battlePage;
		}
		
		public static function addToPanel(vehID:int):void
		{
			var obse:Sprite = new Sprite();
			obse.name = "battleОbserver";
			obse.x = 380;
			var holder:* = _battlePage.playersPanel.listLeft.getHolderByVehicleID(vehID);
			if (!holder)
			{
				obse.x = -obse.x;
				holder = _battlePage.playersPanel.listRight.getHolderByVehicleID(vehID);
			}
			
			holder._listItem.addChildAt(obse, holder._listItem.getChildIndex(holder._listItem.vehicleTF) + 1);
			panels[vehID] = obse;
			listItems[vehID] = new Object();
		}
		
		public static function setNewHealth(vehID:int, percent:Number, text:String):void
		{
			Animation.runAnimation(listItems[vehID][vehID], percent);
			listItems[vehID]["hpText"].htmlText = text;
		}
		
		public static function drawHPbar(vehID:int, bgColor:uint, bgAlpha:Number, hpColor:uint, hpAlpha:Number, xpos:int, ypos:int, width:int, height:int, team:String):void
		{
			
			var hpBarBG:Sprite = new Sprite();
			hpBarBG.name = "hpBarBG";
			hpBarBG.x = team == "red" ? -xpos : xpos;
			hpBarBG.y = ypos;
			hpBarBG.graphics.beginFill(bgColor, bgAlpha);
			hpBarBG.graphics.drawRect(0, 0, team == "red" ? -width : width, height);
			hpBarBG.graphics.endFill();
			panels[vehID].addChild(hpBarBG);
			listItems[vehID][hpBarBG.name] = hpBarBG;
			
			var bar:Shape = new Shape();
			bar.name = vehID.toString();
			bar.x = team == "red" ? -1 : 1;
			bar.y = 1;
			bar.alpha = hpAlpha;
			bar.graphics.beginFill(hpColor, 1);
			bar.graphics.drawRect(0, 0, team == "red" ? -width + 2 : width - 2, height - 2);
			bar.graphics.endFill();
			hpBarBG.addChild(bar);
			listItems[vehID][bar.name] = bar;
			addTextField(vehID, "hpText", width / 2, -2, TextFieldAutoSize.CENTER, team, "hpBarBG");
		}
		
		public static function addTextField(vehID:int, name:String, xpos:int, ypos:int, align:String, team:String, target:String = ""):void
		{
			var texF:TextField = new TextField();
			texF.width = 1;
			texF.x = team == "red" ? -xpos : xpos;
			texF.y = ypos;
			texF.name = name;
			texF.visible = name != "DamageTf";
			texF.selectable = false;
			if (team == "green")
			{
				texF.autoSize = align == TextFieldAutoSize.CENTER ? align : align == TextFieldAutoSize.LEFT ? TextFieldAutoSize.LEFT : TextFieldAutoSize.RIGHT;
			}
			else
			{
				texF.autoSize = align == TextFieldAutoSize.CENTER ? align : align == TextFieldAutoSize.LEFT ? TextFieldAutoSize.RIGHT : TextFieldAutoSize.LEFT;
			}
			texF.antiAliasType = AntiAliasType.ADVANCED;
			texF.gridFitType = GridFitType.SUBPIXEL;
			texF.defaultTextFormat = logsformat;
			texF.filters = [glow];
			if (!target)
			{
				panels[vehID].addChild(texF);
			}
			else
			{
				listItems[vehID][target].addChild(texF);
			}
			listItems[vehID][name] = texF;
		}
		
		public static function updateTextField(vehID:int, feald:String, text:String):void
		{
			listItems[vehID][feald].htmlText = text;
		}
		
		public static function setVehicleClassColor(vehID:int, color:String):void
		{
			var holder:* = _battlePage.playersPanel.listLeft.getHolderByVehicleID(vehID);
			if (!holder)
			{
				holder = _battlePage.playersPanel.listRight.getHolderByVehicleID(vehID);
			}
			holder._listItem.vehicleTF.htmlText = "<b><font color='" + color + "'>" + holder._listItem.vehicleTF.text + "</font></b>";
		}
		
		public static function setHPbarsVisible(vehID:int, vis:Boolean):void
		{
			listItems[vehID]["hpBarBG"].visible = vis;
		}
		
		public static function colorBlindBarEnable(vehID:int, hpColor:uint):void
		{
			var target:Shape = listItems[vehID][vehID];
			var colorInfo:ColorTransform = target.transform.colorTransform;
			colorInfo.color = hpColor;
			target.transform.colorTransform = colorInfo;
		}
		
		public static function onDispose():void
		{
			listItems = null;
			panels = null;
			_battlePage = null;
		}
		
		public static function setDamageVisible(vehID:int, vis:Boolean):void
		{
			listItems[vehID]["DamageTf"].visible = vis;
		}
	}

}