package battle
{
	public class Minimap
	{
		private static var minimap:* = null;
		private static var oldX:Number = 0.0;
		private static var oldY:Number = 0.0;
		private static var oldSize:Number = 0.0;
		
		public function Minimap()
		{
			super();
		}
		
		public static function setBattlePage(battlePage:*):void
		{
			minimap = battlePage.hasOwnProperty("minimap") ? battlePage.minimap : null;
		}
		
		public static function updateMinimap(enabled:Boolean):void
		{
			if (minimap)
			{
				if (enabled)
				{
					oldSize = minimap.currentSizeIndex;
					oldX = minimap.x;
					oldY = minimap.y;
					minimap.as_setSize(5.0);
					minimap.x = App.appWidth * 0.5 - 320;
					minimap.y = App.appHeight * 0.5 - 320;
				}
				else
				{
					minimap.as_setSize(oldSize);
					minimap.x = oldX;
					minimap.y = oldY;
				}
			}
		}
	}

}