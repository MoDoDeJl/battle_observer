﻿package battle
{
	import flash.display.*;
	import flash.filters.*;
	import flash.text.*;
	import net.wg.gui.battle.components.*;
	import battle.Animation;
	import battle.HPanels;
	import battle.Minimap;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	
	public class ModBattleObserverUI extends BattleUIDisplayable
	{
		public static var flashLogS:Function;
		private var objects:Object = new Object;
		private var _battlePage:* = null;
		private var allyColor:uint = 0;
		private var enemyColor:uint = 0;
		private var style:String = null;
		private var inlogYcache:Number = 235;
		private var hpBarsAlpha:Number = 0.7;
		private var bgBarsAlpha:Number = 0.5;
		private var bgBarsColor:uint = 0;
		private var hpBarsEnabled:Boolean = true;
		private var barsWidth:Number = 200;
		private var TimerCanvas:Sprite = new Sprite();
		private var ArmorCalculatorCanvas:Sprite = new Sprite();
		private var DamageLogsCanvas:Sprite = new Sprite();
		private var TeamBasesCanvas:Sprite = new Sprite();
		private var TeamsHpCanvas:Sprite = new Sprite();
		private var ScoreCanvas:Sprite = new Sprite();
		private var allyArrowVector:Vector.<Number> = new <Number>[-6, 7, 0, 7, 6, 15, 0, 23, -6, 23, 0, 15, -6, 7];
		private var enemyArrowVector:Vector.<Number> = new <Number>[6, 7, 0, 7, -6, 15, 0, 23, 6, 23, 0, 15, 6, 7];
		private var arrowCommands:Vector.<int> = new <int>[1, 2, 2, 2, 2, 2, 2];
		private var defCommadsVector:Vector.<int> = new <int>[1, 2, 2, 2, 2];
		private var whiteC:uint = 0xFAFAFA;
		private var normal_style:TextFormat = new TextFormat("$TitleFont", 18, whiteC);
		private var logsformat:TextFormat = new TextFormat("$TitleFont", 15, whiteC);
		private var topformat:TextFormat = new TextFormat("$TitleFont", 20, whiteC);
		private var glow:GlowFilter = new GlowFilter(0, 0.85, 2, 2, 1.9, BitmapFilterQuality.LOW, false, false);
		private var glowScore:GlowFilter = new GlowFilter(0, 1, 6, 6, 2, BitmapFilterQuality.LOW, false, false);
		
		public function ModBattleObserverUI(battlePageUI:*, compName:String = '')
		{
			this.name = compName;
			_battlePage = battlePageUI;
			HPanels.setBattlePage(battlePageUI);
			Minimap.setBattlePage(battlePageUI);
			super();
		}
		
		override protected function onPopulate():void
		{
			super.onPopulate();
			this._battlePage.addEventListener(Event.RESIZE, this.onResize);
		}
		
		override protected function onDispose():void
		{
			this._battlePage.removeEventListener(Event.RESIZE, this.onResize);
			this._battlePage = null;
			objects = null;
			HPanels.onDispose();
			Animation.onDispose();
			super.onDispose();
		}
		
		public function as_MinimapCentered(enabled:Boolean):void
		{
			Minimap.updateMinimap(enabled);
		}
		
		public static function flashLog(param1:*):void
		{
			flashLogS(param1);
		}
		
		public function as_AddVehIdToList(vehicleid:int):void
		{
			HPanels.addToPanel(vehicleid);
		}
		
		public function as_AddTextField(vehicleid:int, name:String, xpos:int, ypos:int, align:String, team:String):void
		{
			HPanels.addTextField(vehicleid, name, xpos, ypos, align, team);
		}
		
		public function as_AddPPanelBar(vehicleid:int, color:uint, xpos:int, ypos:int, width:int, height:int, team:String):void
		{
			HPanels.drawHPbar(vehicleid, bgBarsColor, bgBarsAlpha, color, hpBarsAlpha, xpos, ypos, width, height, team);
		}
		
		public function as_updateTextField(vehicleid:int, feald:String, text:String):void
		{
			HPanels.updateTextField(vehicleid, feald, text);
		}
		
		public function as_updatePPanelBar(vehicleid:int, curr:int, max:int, text:String):void
		{
			HPanels.setNewHealth(vehicleid, curr / max, text);
		}
		
		public function as_setVehicleClassColor(vehID:int, color:String):void
		{
			HPanels.setVehicleClassColor(vehID, color);
		}
		
		public function as_setHPbarsVisible(vehID:int, visible:Boolean):void
		{
			HPanels.setHPbarsVisible(vehID, visible);
		}
		
		public function as_setPlayersDamageVisible(vehID:int, visible:Boolean):void
		{
			HPanels.setDamageVisible(vehID, visible);
		}
		
		public function as_startUpdate(data:Object):void
		{
			style = data['style'];
			setBackground(data['bg']['bg_alpha'], data['bg']['bg_vis'], data['bg']['user_bg']);
			createTimers();
			createArmorCalculatorSprite();
			objects["armorCalc"].x = data['calc']['cx'];
			objects["armorCalc"].y = data['calc']['cy'];
			objects["armorJoke"].x = data['calc']['tx'];
			objects["armorJoke"].y = data['calc']['ty'];
			objects["flyTime"].x = data['calc']['fx'];
			objects["flyTime"].y = data['calc']['fy'];
			allyColor = data['colors']['ally'];
			enemyColor = data['colors']['enemy'];
			createLogsSpriteS();
			objects["damage"].x = data['logsPos']['tx'];
			objects["damage"].y = data['logsPos']['ty'];
			objects["damage"].scaleX = data['logsPos']['scale'];
			objects["damage"].scaleY = data['logsPos']['scale'];
			objects["mainGun"].x = App.appWidth / 2 + data['logsPos']['mgx'];
			objects["mainGun"].y = data['logsPos']['mgy'];
			objects["d_log"].x = data['logsPos']['dx'];
			objects["d_log"].y = data['logsPos']['dy'];
			objects["in_log"].x = data['logsPos']['ix'];
			objects["in_log"].y = App.appHeight - data['logsPos']['iy'];
			inlogYcache = data['logsPos']['iy'];
			bgBarsAlpha = data['bars']['alpha'];
			bgBarsColor = data['bars']['bgHpColor'];
			hpBarsAlpha = data['bars']['hpBarsAlpha'];
			hpBarsEnabled = data['bars']['enabled'];
			createDebugPanel();
			objects["debugPanel"].scaleX = data['debug']['scale'];
			objects["debugPanel"].scaleY = data['debug']['scale'];
			objects["debugPanel"].x = data['debug']['x'];
			objects["debugPanel"].y = data['debug']['y'];
			barsWidth = data['bars']['width'];
			createTeamSHPSprite();
			createScoreSprite();
			objects["allyM"].x = data['markers']['ax'];
			objects["allyM"].y = data['markers']['ay'];
			objects["enemyM"].x = data['markers']['ex'] - 1;
			objects["enemyM"].y = data['markers']['ey'];
			createBasesSprite();
			TeamBasesCanvas.x = App.appWidth / 2 - 200;
			TeamBasesCanvas.y += data['bases']['y'];
			TeamBasesCanvas.scaleX = data['bases']['scale'];
			TeamBasesCanvas.scaleY = data['bases']['scale'];
			App.utils.data.cleanupDynamicObject(data);
		}
		
		private function createTimers():void
		{
			this.addChild(this.TimerCanvas);
			TimerCanvas.x = App.appWidth;
			createTextField("clock", -80, 0, topformat, AntiAliasType.NORMAL, TextFieldAutoSize.RIGHT, TimerCanvas);
			createTextField("timer", -8, 0, topformat, AntiAliasType.NORMAL, TextFieldAutoSize.RIGHT, TimerCanvas);
			objects["clock"].multiline = true;
			objects["timer"].multiline = true;
		}
		
		private function createDebugPanel():void
		{
			var newDebugPanel:Sprite = new Sprite();
			this.addChild(newDebugPanel);
			
			createTextField("debugPanel", 5, 0, topformat, AntiAliasType.NORMAL, TextFieldAutoSize.LEFT, newDebugPanel);
			objects["debugPanel"].multiline = true;
		}
		
		private function createLogsSpriteS():void
		{
			this.addChild(DamageLogsCanvas);
			createTextField("d_log", 0, 0, logsformat, AntiAliasType.ADVANCED, TextFieldAutoSize.LEFT, DamageLogsCanvas);
			objects["d_log"].multiline = true;
			objects["d_log"].gridFitType = GridFitType.SUBPIXEL;
			createTextField("in_log", 0, 0, logsformat, AntiAliasType.ADVANCED, TextFieldAutoSize.LEFT, DamageLogsCanvas);
			objects["in_log"].multiline = true;
			objects["in_log"].gridFitType = GridFitType.SUBPIXEL;
			createTextField("damage", 0, 0, topformat, AntiAliasType.NORMAL, TextFieldAutoSize.LEFT, DamageLogsCanvas);
			objects["damage"].multiline = true;
			createTextField("mainGun", 0, 0, topformat, AntiAliasType.NORMAL, TextFieldAutoSize.LEFT, DamageLogsCanvas);
			objects["mainGun"].multiline = true;
		}
		
		private function createArmorCalculatorSprite():void
		{
			this.addChild(ArmorCalculatorCanvas);
			this.ArmorCalculatorCanvas.x = App.appWidth >> 1;
			this.ArmorCalculatorCanvas.y = App.appHeight >> 1;
			
			createTextField("armorCalc", 0, 0, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.CENTER, ArmorCalculatorCanvas);
			objects["armorCalc"].multiline = true;
			objects["armorCalc"].gridFitType = GridFitType.SUBPIXEL;
			
			createTextField("flyTime", 0, 0, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.CENTER, ArmorCalculatorCanvas);
			objects["flyTime"].multiline = true;
			objects["flyTime"].gridFitType = GridFitType.SUBPIXEL;
			
			createTextField("armorJoke", 0, 0, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.CENTER, ArmorCalculatorCanvas);
			objects["armorJoke"].multiline = true;
			objects["armorJoke"].gridFitType = GridFitType.SUBPIXEL;
		}
		
		private function createScoreSprite():void
		{
			this.addChild(ScoreCanvas);
			ScoreCanvas.x = App.appWidth >> 1;
			
			var arrowDots:Shape = new Shape();
			arrowDots.name = "arrowDots";
			arrowDots.x = 1;
			arrowDots.filters = [glowScore];
			arrowDots.graphics.beginFill(0xFAFAFA, 1);
			arrowDots.graphics.drawCircle(0, 10, 3);
			arrowDots.graphics.drawCircle(0, 21, 3);
			arrowDots.graphics.endFill();
			arrowDots.visible = false;
			this.ScoreCanvas.addChild(arrowDots);
			
			var arrowGreen:Shape = new Shape();
			arrowGreen.name = "arrowGreen";
			arrowGreen.x = 1;
			arrowGreen.filters = [glowScore];
			arrowGreen.graphics.beginFill(allyColor, 1);
			arrowGreen.graphics.drawPath(arrowCommands, allyArrowVector);
			arrowGreen.graphics.endFill();
			arrowGreen.visible = false;
			this.ScoreCanvas.addChild(arrowGreen);
			
			var arrowRed:Shape = new Shape();
			arrowRed.name = "arrowRed";
			arrowRed.x = 1;
			arrowRed.filters = [glowScore];
			arrowRed.graphics.beginFill(enemyColor, 1);
			arrowRed.graphics.drawPath(arrowCommands, enemyArrowVector);
			arrowRed.graphics.endFill();
			arrowRed.visible = false;
			this.ScoreCanvas.addChild(arrowRed);
			
			var scoreformat:TextFormat = new TextFormat("$TitleFont", 24, whiteC, true);
			var markersFormat:TextFormat = new TextFormat("BattleObserver", 26, whiteC);
			markersFormat.letterSpacing = 1;
			
			createTextField("greenScore", -26, -3, scoreformat, AntiAliasType.NORMAL, TextFieldAutoSize.CENTER, ScoreCanvas);
			objects["greenScore"].filters = [glowScore];
			
			createTextField("redScore", 24, -3, scoreformat, AntiAliasType.NORMAL, TextFieldAutoSize.CENTER, ScoreCanvas);
			objects["redScore"].filters = [glowScore];
			
			createTextField("allyM", 0, 0, markersFormat, AntiAliasType.ADVANCED, TextFieldAutoSize.RIGHT, ScoreCanvas);
			objects["allyM"].multiline = true;
			objects["allyM"].embedFonts = true;
			objects["allyM"].gridFitType = GridFitType.SUBPIXEL;
			
			createTextField("enemyM", 0, 0, markersFormat, AntiAliasType.ADVANCED, TextFieldAutoSize.LEFT, ScoreCanvas);
			objects["enemyM"].multiline = true;
			objects["enemyM"].embedFonts = true;
			objects["enemyM"].gridFitType = GridFitType.SUBPIXEL;
			
			objects["arrowDots"] = arrowDots;
			objects["arrowGreen"] = arrowGreen;
			objects["arrowRed"] = arrowRed;
		}
		
		private function createTeamSHPSprite():void
		{
			if (hpBarsEnabled)
			{
				this.addChild(TeamsHpCanvas);
				TeamsHpCanvas.x = 1 + (App.appWidth >> 1);
				
				drawHpBars();
				
				createTextField("greenText", -50 - (barsWidth / 2), 0, normal_style, AntiAliasType.NORMAL, TextFieldAutoSize.CENTER, TeamsHpCanvas);
				createTextField("redText", 50 + (barsWidth / 2), 0, normal_style, AntiAliasType.NORMAL, TextFieldAutoSize.CENTER, TeamsHpCanvas);
				
				if (style == "legue")
				{
					var mainTextShape:Shape = new Shape();
					mainTextShape.name = "logShape";
					this.TeamsHpCanvas.addChild(mainTextShape);
					objects["logShape"] = mainTextShape;
					createTextField("log", 0, 28, new TextFormat("$TitleFont", 20, whiteC, true), AntiAliasType.ADVANCED, TextFieldAutoSize.CENTER, TeamsHpCanvas);
					objects["log"].gridFitType = GridFitType.SUBPIXEL;
					
					createTextField("greenDiff", -50, 1, normal_style, AntiAliasType.NORMAL, TextFieldAutoSize.RIGHT, TeamsHpCanvas);
					createTextField("redDiff", 50, 1, normal_style, AntiAliasType.NORMAL, TextFieldAutoSize.LEFT, TeamsHpCanvas);
					
					objects["greenText"].x = -barsWidth + 20;
					objects["redText"].x = barsWidth - 20;
				}
				else
				{
					createTextField("diff", 0, 26, normal_style, AntiAliasType.NORMAL, TextFieldAutoSize.CENTER, TeamsHpCanvas);
				}
			}
		}
		
		private function setBackground(alpha:Number, visible:Boolean, data:Array):void
		{
			if (visible && style == "normal")
			{
				var backgroundX:Shape = new Shape();
				backgroundX.alpha = alpha;
				backgroundX.graphics.beginFill(0, alpha);
				backgroundX.graphics.drawRect(0, 0, App.appWidth, 31);
				backgroundX.graphics.endFill();
				this.addChild(backgroundX);
			}
			else if (data[2] is String)
			{
				var bg:TextField = new TextField;
				bg.x += data[0];
				bg.y += data[1];
				bg.selectable = false;
				bg.autoSize = TextFieldAutoSize.LEFT;
				bg.htmlText = data[2];
				this.addChild(bg);
			}
		}
		
		private function drawHpBars():void
		{
			var hpBars_bg:Shape = new Shape();
			this.TeamsHpCanvas.addChild(hpBars_bg);
			
			var allyHpBar:Shape = new Shape();
			allyHpBar.name = "greenBar";
			allyHpBar.alpha = hpBarsAlpha;
			this.TeamsHpCanvas.addChild(allyHpBar);
			
			var enemyHpBar:Shape = new Shape();
			enemyHpBar.name = "redBar";
			enemyHpBar.alpha = hpBarsAlpha;
			this.TeamsHpCanvas.addChild(enemyHpBar);
			
			if (style == "normal")
			{
				hpBars_bg.graphics.beginFill(bgBarsColor, bgBarsAlpha);
				hpBars_bg.graphics.drawRect(-49, 3, -(barsWidth + 2), 24);
				hpBars_bg.graphics.drawRect(49, 3, barsWidth + 2, 24);
				hpBars_bg.graphics.endFill();
				hpBars_bg.graphics.lineStyle(0, 0xFFFFFF, 1, true);
				hpBars_bg.graphics.drawRect(-49, 2, -(barsWidth + 3), 25);
				hpBars_bg.graphics.drawRect(48, 2, barsWidth + 3, 25);
				allyHpBar.x = -50;
				allyHpBar.graphics.beginFill(allyColor, 1);
				allyHpBar.graphics.drawRect(0, 4, -barsWidth, 22);
				allyHpBar.graphics.endFill();
				enemyHpBar.x = 50;
				enemyHpBar.graphics.beginFill(enemyColor, 1);
				enemyHpBar.graphics.drawRect(0, 4, barsWidth, 22);
				enemyHpBar.graphics.endFill();
			}
			else
			{
				var xpos:Number = 20 + barsWidth;
				var height:Number = 31;
				var coords:Vector.<Number> = new <Number>[-xpos, 0, xpos, 0, xpos - 15, height, -xpos + 15, height, -xpos, 0];
				hpBars_bg.graphics.beginFill(bgBarsColor, bgBarsAlpha);
				hpBars_bg.graphics.drawPath(defCommadsVector, coords);
				hpBars_bg.graphics.endFill();
				allyHpBar.x = -1;
				var coords_ally:Vector.<Number> = new <Number>[0, 0, -xpos, 0, -xpos + 15, height, 0, height, 0, 0];
				allyHpBar.graphics.beginFill(allyColor, 1);
				allyHpBar.graphics.drawPath(defCommadsVector, coords_ally);
				allyHpBar.graphics.endFill();
				enemyHpBar.x = 0;
				var coords_enemy:Vector.<Number> = new <Number>[0, 0, xpos, 0, xpos - 15, height, 0, height, 0, 0];
				enemyHpBar.graphics.beginFill(enemyColor, 1);
				enemyHpBar.graphics.drawPath(defCommadsVector, coords_enemy);
				enemyHpBar.graphics.endFill();
			}
			objects["greenBar"] = allyHpBar;
			objects["redBar"] = enemyHpBar;
		}
		
		private function createBasesSprite():void
		{
			this.addChild(TeamBasesCanvas);
			
			var allyBase_bg:Sprite = new Sprite();
			allyBase_bg.name = "greenBaseMain";
			allyBase_bg.visible = false;
			allyBase_bg.graphics.beginFill(bgBarsColor, bgBarsAlpha);
			allyBase_bg.graphics.drawRect(0, 10, 400, 30);
			allyBase_bg.graphics.endFill();
			this.TeamBasesCanvas.addChild(allyBase_bg);
			
			var allyBase:Shape = new Shape();
			allyBase.name = "greenBaseBar";
			allyBase.graphics.beginFill(allyColor, hpBarsAlpha);
			allyBase.graphics.drawRect(0, 10, 400, 30);
			allyBase.graphics.endFill();
			allyBase.scaleX = 0.0;
			allyBase_bg.addChild(allyBase);
			
			objects["greenBaseMain"] = allyBase_bg;
			objects["greenBaseBar"] = allyBase;
			createTextField("greenBaseText", 200, 11, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.CENTER, allyBase_bg);
			createTextField("greenBaseTimer", 400, 9, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.RIGHT, allyBase_bg);
			createTextField("greenBaseVehicles", -1, 9, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.LEFT, allyBase_bg);
			
			var enemyBase_bg:Sprite = new Sprite();
			enemyBase_bg.name = "redBaseMain";
			enemyBase_bg.visible = false;
			enemyBase_bg.graphics.beginFill(bgBarsColor, bgBarsAlpha);
			enemyBase_bg.graphics.drawRect(0, 50, 400, 30);
			enemyBase_bg.graphics.endFill();
			this.TeamBasesCanvas.addChild(enemyBase_bg);
			
			var enemyBase:Shape = new Shape();
			enemyBase.name = "redBaseBar";
			enemyBase.alpha = hpBarsAlpha;
			enemyBase.graphics.beginFill(enemyColor, 1);
			enemyBase.graphics.drawRect(0, 50, 400, 30);
			enemyBase.graphics.endFill();
			enemyBase.scaleX = 0.0;
			enemyBase_bg.addChild(enemyBase);
			
			objects["redBaseMain"] = enemyBase_bg;
			objects["redBaseBar"] = enemyBase;
			createTextField("redBaseText", 200, 51, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.CENTER, enemyBase_bg);
			createTextField("redBaseTimer", 400, 49, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.RIGHT, enemyBase_bg);
			createTextField("redBaseVehicles", -1, 49, normal_style, AntiAliasType.ADVANCED, TextFieldAutoSize.LEFT, enemyBase_bg);
		}
		
		private function createTextField(name:String, x:Number, y:Number, style:TextFormat, antiAlias:String, align:String, object:Sprite):void
		{
			var feald:TextField = new TextField();
			feald.x = x;
			feald.y = y;
			feald.width = 1;
			feald.name = name;
			feald.selectable = false;
			feald.autoSize = align;
			feald.defaultTextFormat = style;
			feald.antiAliasType = antiAlias;
			feald.filters = [glow];
			object.addChild(feald);
			objects[name] = feald;
		}
		
		public function as_colorBlindPPbars(vehID:int, color:uint):void
		{
			HPanels.colorBlindBarEnable(vehID, color);
		}
		
		public function as_colorBlind(color:uint):void
		{
			enemyColor = color;
			var enemyHpBar:Shape = objects["redBar"];
			var colorInfo:ColorTransform = enemyHpBar.transform.colorTransform;
			colorInfo.color = enemyColor;
			enemyHpBar.transform.colorTransform = colorInfo;
			
			var enemyBase:Shape = objects["redBaseBar"];
			colorInfo = enemyBase.transform.colorTransform;
			colorInfo.color = enemyColor;
			enemyBase.transform.colorTransform = colorInfo;
			
			var arrow:Shape = objects["arrowRed"];
			colorInfo = arrow.transform.colorTransform;
			colorInfo.color = enemyColor;
			arrow.transform.colorTransform = colorInfo;
		}
		
		public function as_flyghtTime(text:String):void
		{
			objects["flyTime"].htmlText = text;
		}
		
		public function as_armorCalc(armor:String):void
		{
			objects["armorCalc"].htmlText = armor;
		}
		
		public function as_armorTextS(text:String):void
		{
			objects["armorJoke"].htmlText = text;
		}
		
		public function as_updateLog(target:String, text:String):void
		{
			objects[target].htmlText = text;
		}
		
		public function as_updateDamage(text:String):void
		{
			if (style == 'legue' && hpBarsEnabled)
			{
				var mainText:TextField = objects["log"];
				var mainTextShape:Shape = objects["logShape"];
				mainText.htmlText = text;
				var wid:int = (mainText.width / 2) + 15;
				var coords:Vector.<Number> = new <Number>[-wid, 31, wid, 31, wid - 15, 28 + mainText.height, -wid + 15, 28 + mainText.height, -wid, 31];
				mainTextShape.graphics.clear();
				mainTextShape.graphics.beginFill(bgBarsColor, bgBarsAlpha);
				mainTextShape.graphics.drawPath(defCommadsVector, coords);
				mainTextShape.graphics.endFill();
			}
			else
			{
				objects["damage"].htmlText = text;
			}
		}
		
		public function as_mainGunText(GunText:String):void
		{
			if (style == 'legue' && hpBarsEnabled)
			{
				objects["mainGun"].visible = false;
			}
			else
			{
				objects["mainGun"].htmlText = GunText;
			}
		}
		
		public function up_fpsPing(debug:String):void
		{
			objects["debugPanel"].htmlText = debug;
		}
		
		public function as_UpdateTime(clock:String, timer:String):void
		{
			objects["clock"].htmlText = clock;
			objects["timer"].htmlText = timer;
		}
		
		public function as_updateScore(ally:int, enemy:int):void
		{
			objects["arrowDots"].visible = ally == enemy;
			objects["arrowGreen"].visible = ally > enemy;
			objects["arrowRed"].visible = ally < enemy;
			objects["greenScore"].text = ally.toString();
			objects["redScore"].text = enemy.toString();
		}
		
		public function up_markers(ally:String, enemy:String):void
		{
			objects["allyM"].htmlText = ally;
			objects["enemyM"].htmlText = enemy;
		}
		
		public function as_setBaseVisible(base:String, visible:Boolean):void
		{
			objects[base + "BaseMain"].visible = visible;
			if (!visible)
			{
				Animation.runBaseAnimation(objects[base + "BaseBar"], 0.0);
			}
		}
		
		public function as_updateBases(param:String, points:Number, invadersCnt:int, time:String):void
		{
			Animation.runBaseAnimation(objects[param + "BaseBar"], points);
			objects[param + "BaseTimer"].htmlText = time + "<img src='img://gui/maps/icons/vehicleStates/rentalIsOver.png' width='28' height='28' vspace='-9'>";
			objects[param + "BaseVehicles"].htmlText = "<img src='img://gui/maps/icons/vehicleStates/crewNotFull.png' width='28' height='28' vspace='-9'>" + invadersCnt.toString();
		}
		
		public function as_updateBaseText(param:String, text:String):void
		{
			objects[param + "BaseText"].htmlText = text;
		}
		
		public function as_updateHealth(team:String, barPercent:Number, healths:int):void
		{
			if (hpBarsEnabled)
			{
				Animation.runAnimation(objects[team + "Bar"], barPercent);
				objects[team + "Text"].text = healths.toString();
			}
		}
		
		public function as_difference(diff:int):void
		{
			var data:String = diff < 0 ? ( -diff).toString() : diff.toString();
			objects[style != "normal" ? diff < 0 ? "redDiff" : "greenDiff" : "diff"].text = style == "normal" ? data : "+" + data;
			objects[style != "normal" ? diff < 0 ? "redDiff" : "greenDiff" : "diff"].textColor = style == "normal" ? diff < 0 ? enemyColor : allyColor : whiteC;
			if (style != "normal")
			{
				objects[diff < 0 ? "greenDiff" : "redDiff"].text = "";
			}
		}
		
		private function onResize():void
		{
			var C_WIDTH:Number = App.appWidth >> 1;
			var C_HEIGHT:Number = App.appHeight >> 1;
			this.ArmorCalculatorCanvas.x = C_WIDTH;
			this.ArmorCalculatorCanvas.y = C_HEIGHT;
			this.objects["in_log"].y = App.appHeight - inlogYcache;
			this.objects["mainGun"].x = C_WIDTH;
			this.TimerCanvas.x = App.appWidth;
			this.ScoreCanvas.x = C_WIDTH;
			this.TeamBasesCanvas.x = C_WIDTH - 200;
			this.TeamsHpCanvas.x = C_WIDTH + 1;
		}
	}
}