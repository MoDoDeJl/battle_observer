# -*- coding: utf-8 -*-
import BattleReplay
from AvatarInputHandler.DynamicCameras import SniperCamera, ArcadeCamera, StrategicCamera
from AvatarInputHandler.control_modes import SniperControlMode, PostMortemControlMode
from account_helpers.settings_core.settings_constants import GAME
from Core import g_settingsCore
from Config import cfg
from BoEvents import eve

SNIPER_ENABLE = []
SNIPER_READCFG = SniperCamera.SniperCamera._SniperCamera__readCfg
STRATEGIC_READCFG = StrategicCamera.StrategicCamera._StrategicCamera__readCfg
ARCADE_READCFG = ArcadeCamera.ArcadeCamera._ArcadeCamera__readCfg
POSTMORTEM_ENABLE = PostMortemControlMode.enable

class Camera(object):
    __slots__ = ('MinMax', 'enableX', 'zoomSteps', 'sCam')
    def __init__(self):
        g_settingsCore.onSettingsApplied += self.onSettingsApplied
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle
        self.enableX = cfg.zoom['enabled']
        self.zoomSteps = cfg.zoom['zoomSteps']
        self.sCam = None
        self.__hooks()

    def battleLoading(self, arenaDP, player, teams, isSPG):
        self.enableX = cfg.zoom['enabled']
        if self.enableX and not BattleReplay.isPlaying():
            d_zoom = cfg.zoom['dynamyc_zoom']
            dynamyc_zoom = d_zoom['enable']
            defZoom = cfg.zoom['default_zoom']['defZoom']
            minZoom = max(2.0, d_zoom['minZoom'])
            maxZoom = min(40.0, d_zoom['maxZoom'])
            zoomXMetrs = max(10, d_zoom['zoomXMetrs'])
            zoomToGunMarker = d_zoom['zoomToGunMarker']
            default_zoom = cfg.zoom['default_zoom']['enable']
            self.zoomSteps = cfg.zoom['zoomSteps']
            SNIPER_ENABLE.append(SniperCamera.SniperCamera.enable)
            SniperCamera.SniperCamera.enable = lambda b_self, targetPos, saveZoom: self.sCam_enable(b_self, targetPos, saveZoom, dynamyc_zoom, defZoom, minZoom, maxZoom, zoomXMetrs, zoomToGunMarker, default_zoom, player)

    def destroyBattle(self):
        if self.enableX and not BattleReplay.isPlaying():
            SniperCamera.SniperCamera.enable = SNIPER_ENABLE.pop(0)

    def __hooks(self):
        SniperCamera.SniperCamera._SniperCamera__readCfg = lambda b_self, dataSec: self.sniperCfg(b_self, dataSec)
        StrategicCamera.StrategicCamera._StrategicCamera__readCfg = lambda b_self, dataSec: self.strategicCfg(b_self, dataSec)
        ArcadeCamera.ArcadeCamera._ArcadeCamera__readCfg = lambda b_self, dataSec: self.arcadeCfg(b_self, dataSec)
        PostMortemControlMode.enable = lambda b_self, **args: self.postmortemEnable(b_self, **args)
        if self.enableX and not cfg.zoom['LENS_EFFECTS_ENABLED']:
            SniperControlMode._SniperControlMode__setupBinoculars = lambda *args: None
            SniperControlMode._LENS_EFFECTS_ENABLED = cfg.zoom['LENS_EFFECTS_ENABLED']

    def postmortemEnable(self, b_self, **args):
        if cfg.arcadeCamera['enabled']:
            b_self._PostMortemControlMode__cam._ArcadeCamera__cfg['distRange'] = ArcadeCamera.MinMax(min=round(cfg.arcadeCamera['min']), max=round(cfg.arcadeCamera['startDeadDist']))
            POSTMORTEM_ENABLE(b_self, **args)
            b_self._PostMortemControlMode__cam._ArcadeCamera__cfg['distRange'] = ArcadeCamera.MinMax(min=round(cfg.arcadeCamera['min']), max=round(cfg.arcadeCamera['max']))
        else:
            return POSTMORTEM_ENABLE(b_self, **args)

    def sCam_enable(self, b_self, targetPos, saveZoom, dynamyc_zoom, defZoom, minZoom, maxZoom, zoomXMetrs, zoomToGunMarker, default_zoom, player):
        if zoomToGunMarker:
            targetPos = player.gunRotator.markerInfo[0]
        if dynamyc_zoom:
            dist = targetPos.flatDistTo(player.position)
            b_self._SniperCamera__cfg['zoom'] = round(max(min(dist / zoomXMetrs, maxZoom), minZoom)) if dist <= 560.0 else defZoom
        elif default_zoom:
            b_self._SniperCamera__cfg['zoom'] = defZoom
        SNIPER_ENABLE[0](b_self, targetPos, True)

    def onSettingsApplied(self, diff):
        if not BattleReplay.isPlaying() and GAME.INCREASED_ZOOM in diff and self.enableX and self.sCam:
            self.sCam._SniperCamera__cfg['increasedZoom'] = True
            self.sCam._SniperCamera__cfg['zooms'] = self.zoomSteps
            self.sCam._SniperCamera__zoom = self.zoomSteps[0]
            self.sCam._SniperCamera__cfg['zoom'] = self.zoomSteps[0]
            self.sCam._SniperCamera__dynamicCfg['zoomExposure'] = [0.5 for i in self.zoomSteps]

    def sniperCfg(self, b_self, dataSec):
        SNIPER_READCFG(b_self, dataSec)
        if self.enableX:
            self.sCam = b_self
            b_self._SniperCamera__cfg['increasedZoom'] = True
            b_self._SniperCamera__cfg['zooms'] = self.zoomSteps
            b_self._SniperCamera__zoom = self.zoomSteps[0]
            b_self._SniperCamera__cfg['zoom'] = self.zoomSteps[0]
            b_self._SniperCamera__dynamicCfg['zoomExposure'] = [0.5 for i in self.zoomSteps]

    def arcadeCfg(self, b_self, dataSec):
        ARCADE_READCFG(b_self, dataSec)
        if cfg.arcadeCamera['enabled']:
            b_self._ArcadeCamera__userCfg['distRange'] = b_self._ArcadeCamera__cfg['distRange'] = ArcadeCamera.MinMax(min=round(cfg.arcadeCamera['min']), max=round(cfg.arcadeCamera['max']))
            b_self._ArcadeCamera__userCfg['startDist'] = b_self._ArcadeCamera__cfg['startDist'] = round(cfg.arcadeCamera['startDeadDist'])

    def strategicCfg(self, b_self, dataSec):
        STRATEGIC_READCFG(b_self, dataSec)
        if cfg.strategicCamera['enabled']:
            b_self._StrategicCamera__userCfg['distRange'] = b_self._StrategicCamera__cfg['distRange'] = (round(cfg.strategicCamera['min']), round(cfg.strategicCamera['max']))