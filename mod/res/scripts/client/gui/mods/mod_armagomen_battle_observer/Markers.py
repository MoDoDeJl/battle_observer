# -*- coding: utf-8 -*-
import BigWorld
from gui.battle_control.arena_info import vos_collections
from account_helpers.settings_core.settings_constants import GAME, GRAPHICS
from Config import cfg
from BoEvents import eve
from Core import g_settingsCore
from BoConstants import MARKERS
getSetting = g_settingsCore.getSetting

class Markers(object):
    __slots__ = ('mcColor', 'vcColor', 'aCol', 'eCol', 'arenaDP', 'ally_collection', 'enemy_collection')
    
    def __init__(self):
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle
        
        for attr in self.__slots__:
            setattr(self, attr, None)

    def battleLoading(self, arenaDP, player, teams, isSPG):
        if cfg.hp_bars["enabled"]:
            eve.onKeyPressed += self.keyEvent
            g_settingsCore.onSettingsApplied += self.onSettingsApplied
            player.arena.onVehicleKilled += self.updateMarkers
            eve.onVehicleAddUpdate += self.updateMarkers
            self.mcColor = cfg.markers['markersClassColor']
            self.vcColor = cfg.vehicle_types['vehicleClassColors']
            self.aCol = (cfg.colors['markers']['deadColor'], cfg.colors['markers']['ally'])
            self.eCol = (cfg.colors['markers']['deadColor'], cfg.colors['markers']['enemy%s' % ('ColorBlind' if getSetting(GRAPHICS.COLOR_BLIND) else '')])
            self.arenaDP = arenaDP
            sort = vos_collections.FragCorrelationSortKey
            self.ally_collection = vos_collections.AllyItemsCollection(sortKey=sort)
            self.enemy_collection = vos_collections.EnemyItemsCollection(sortKey=sort)
            self.updateMarkers()

    def destroyBattle(self):
        if cfg.hp_bars["enabled"]:
            eve.onKeyPressed -= self.keyEvent
            BigWorld.player().arena.onVehicleKilled -= self.updateMarkers
            eve.onVehicleAddUpdate -= self.updateMarkers
            g_settingsCore.onSettingsApplied -= self.onSettingsApplied
        
    def updateMarkers(self, *args):
        self.drawMarkers(getSetting(GAME.SHOW_VEHICLES_COUNTER))
    
    def keyEvent(self, key, isKeyDown):
        if key in cfg.markers['showMarkers_KEY'] and isKeyDown:
            g_settingsCore.applySettings({GAME.SHOW_VEHICLES_COUNTER: not getSetting(GAME.SHOW_VEHICLES_COUNTER)})

    def onSettingsApplied(self, diff):
        if GRAPHICS.COLOR_BLIND in diff:
            self.eCol = (cfg.colors['markers']['deadColor'], cfg.colors['markers']['enemy%s' % ('ColorBlind' if diff[GRAPHICS.COLOR_BLIND] else '')])
            self.drawMarkers(getSetting(GAME.SHOW_VEHICLES_COUNTER))
        elif GAME.SHOW_VEHICLES_COUNTER in diff:
            self.drawMarkers(diff[GAME.SHOW_VEHICLES_COUNTER])

    def drawMarkers(self, enable):
        ally_markers = []
        enemy_markers = []
        if enable:
            for tank, vStatsVO in self.ally_collection.iterator(self.arenaDP):
                isAlive = int(tank.isAlive())
                classTag = tank.vehicleType.classTag
                ally_markers.append(MARKERS.ICON % (self.vcColor[classTag] if self.mcColor and isAlive else self.aCol[isAlive], MARKERS.TYPEICO[classTag]))
            ally_markers.reverse()
            for tank, vStatsVO in self.enemy_collection.iterator(self.arenaDP):
                isAlive = int(tank.isAlive())
                classTag = tank.vehicleType.classTag
                if classTag:
                    enemy_markers.append(MARKERS.ICON % (self.vcColor[classTag] if self.mcColor and isAlive else self.eCol[isAlive], MARKERS.TYPEICO[classTag]))
            if len(ally_markers) > MARKERS.LIST_SIZE:
                ally_markers = ally_markers[MARKERS.LIST_SIZE:] + ['<br>'] + ally_markers[:MARKERS.LIST_SIZE]
                enemy_markers.insert(MARKERS.LIST_SIZE, '<br>')
        eve.componentPy.as_markersS(''.join(ally_markers), ''.join(enemy_markers)) 