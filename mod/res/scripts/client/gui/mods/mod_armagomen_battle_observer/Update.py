# -*- coding: utf-8 -*-
import threading
import urllib2
import random
import traceback
import os
import BigWorld
import zipfile
import StringIO
from gui import DialogsInterface
from gui.Scaleform.daapi.view import dialogs
from gui.Scaleform.daapi.view.login.LoginView import LoginView
from Core import DEFAULT_LANGUAGE, local_ver
from Config import cfg
from i18nBO import geti18nDic

class ConfirmButtons(dialogs.InfoDialogButtons):
    def __init__(self, submit, close):
        super(ConfirmButtons, self).__init__(close)
        self._submit = submit

    def getLabels(self):
        return [{'id': 'submit','label': self._submit,'focused': True}, {'id': 'close','label': self._close,'focused': False}]

class updateDialog(object):
    __slots__ = ()
    
    def __init__(self):
        base_showForm = LoginView._showForm
        LoginView._showForm = lambda base_self: self.login(base_self, base_showForm)

    def login(self, base_self, base):
        base(base_self)
        threading.Thread(target=self.check()).start()
        
    def check(self):
        try:
            update = random.choice(('https://bitbucket.org/Armagomen/battle_observer/downloads/version.txt', 'http://armagomen-mods.at.ua/teamshp_update/version.txt'))
            version = urllib2.urlopen(update)
            ver = version.read()
            version.close()
            if local_ver < ver:
                self.dialog(ver)
        except Exception:
            traceback.print_exc()

    def dialog(self, ver):
        i18nD = geti18nDic(DEFAULT_LANGUAGE)
        title = i18nD.get('UPD_update_title') % ver
        dialogMsg = i18nD.get('UPD_update_dialog') % (local_ver, ver)
        yes = i18nD.get('UPD_update_yes')
        dont = i18nD.get('UPD_update_dont')
        DialogsInterface.showDialog(dialogs.SimpleDialogMeta(title, dialogMsg, ConfirmButtons(yes, dont)), lambda proceed: self.download(i18nD) if proceed else None)

    def download(self, i18nD):
        zFile = urllib2.urlopen('https://bitbucket.org/Armagomen/battle_observer/downloads/BattleObserver_LastUpdate.zip')
        compressedFile = StringIO.StringIO()
        compressedFile.write(zFile.read())
        zFile.close()
        compressedFile.seek(0)
        with zipfile.ZipFile(compressedFile) as zf:
            workingDir = '%s/' % BigWorld.currentModPathBO
            oldFiles = os.listdir(workingDir)
            for newFile in zf.namelist():
                if newFile not in oldFiles:
                    zf.extract(newFile, workingDir)
        self.downloadFinish(i18nD)

    def downloadFinish(self, i18nD):
        title = 'FILE DOWNLOAD COMPLETED'
        dialogMsg = 'The download of the Battle Observer update has been completed.<br>Press OK to restsrt game client now.'
        yes = 'OK'
        dont = i18nD.get('UPD_update_dont')
        DialogsInterface.showDialog(dialogs.SimpleDialogMeta(title, dialogMsg, ConfirmButtons(yes, dont)), lambda proceed: BigWorld.restartGame() if proceed else None)

if cfg.main['CHECK_UPDATES']:
    updateDialog()