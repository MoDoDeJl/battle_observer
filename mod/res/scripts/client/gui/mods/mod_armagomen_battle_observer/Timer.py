# -*- coding: utf-8 -*-
from datetime import datetime
from gui.Scaleform.daapi.view.battle.shared.battle_timers import BattleTimer
from Config import cfg
from BoEvents import eve

BASE_TIMER = []

class Timer(object):
    __slots__ = ('timetemplate', 'timertemplate', 'clockformat')
    def __init__(self):
        for attr in self.__slots__:
            setattr(self, attr, None)
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle

    def __setTotalTime(self, base_self, totalTime, color):
        BASE_TIMER[0](base_self, totalTime)
        time = (self.timetemplate.replace("{{time}}", self.getClock()), self.timertemplate.replace("{{timerColor}}", color[int(totalTime < 120)]).replace("{{timer}}", '%02d:%02d' % (divmod(int(totalTime), 60))))
        eve.componentPy.as_UpdateTimeS(*time)

    def getClock(self):
        return datetime.now().strftime(self.clockformat).decode('windows-1251').encode('utf-8')

    def battleLoading(self, arenaDP, player, teams, isSPG):
        if cfg.battleTimer["enabled"]:
            self.timetemplate = cfg.battleTimer['clockTemplate']
            self.timertemplate = cfg.battleTimer['timerTemplate']
            self.clockformat = cfg.battleTimer['clockFormat']
            color = (cfg.colors['battleTimer']['timerColor'], cfg.colors['battleTimer']['timerColorEndBattle'])
            BASE_TIMER.append(BattleTimer.setTotalTime)
            BattleTimer.setTotalTime = lambda base_self, totalTime: self.__setTotalTime(base_self, totalTime, color)
            eve.componentPy.as_UpdateTimeS(self.timetemplate.replace("{{time}}", self.getClock()), self.timertemplate.replace("{{timerColor}}", "#FAFAFA").replace("{{timer}}", "00:00"))

    def destroyBattle(self):
        if cfg.battleTimer["enabled"] and BASE_TIMER:
            BattleTimer.setTotalTime = BASE_TIMER.pop(0)