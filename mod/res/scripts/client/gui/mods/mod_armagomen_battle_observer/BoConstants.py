# -*- coding: utf-8 -*-
from gui.battle_control.battle_constants import FEEDBACK_EVENT_ID as EV_ID
from vehicle_systems.tankStructure import TankPartIndexes
from constants import ARENA_BONUS_TYPE

EPIC_BATTLE = (ARENA_BONUS_TYPE.EPIC_RANDOM, ARENA_BONUS_TYPE.EPIC_RANDOM_TRAINING, ARENA_BONUS_TYPE.EPIC_BATTLE)
TEAM = {True: "red", False: "green"}

class DAMAGE_LOG:
    MAIN_GUN_INFO, GUN_DYNAMIC, TANKAVGDAMAGE, GUN_TEXT_LEGUE, MG_CHECK, MAXDMG = (0, 1, 2, 3, 4, 5)
    ETYPES = (EV_ID.PLAYER_SPOTTED_ENEMY, EV_ID.PLAYER_DAMAGED_HP_ENEMY, EV_ID.PLAYER_ASSIST_TO_KILL_ENEMY, EV_ID.PLAYER_ASSIST_TO_STUN_ENEMY, EV_ID.PLAYER_USED_ARMOR)
    CONFINES = (0, 30, 60, 100, 150, 200)
    SHELL = ('normal', 'gold')
    CRITS, BLOCK, ASSIST = (0, 1, 2)

class ARMOR_CALC:
    SHELL_DIST_RATE = ('ARMOR_PIERCING', 'ARMOR_PIERCING_HE', 'ARMOR_PIERCING_CR')
    SKIP_DETAILS = (TankPartIndexes.CHASSIS, TankPartIndexes.GUN)
    BACKWARD_LENGTH = 0.1
    FORWARD_LENGTH = 20.0
    MIN_DIST = 100.0

class VEHICLE:
    CUR, MAX, TEAM, CLASSTAG = (0, 1, 2, 3)

class DEBUG:
    MIN, MAX, AVG = (0, 1, 2)

class MARKERS:
    TYPEICO = {"heavyTank":"H", "mediumTank":"M", "AT-SPG":"J", "SPG":"S", "lightTank":"L", "unknown":"U"}
    ICON = "<font color='%s'>%s</font>"
    LIST_SIZE = 15