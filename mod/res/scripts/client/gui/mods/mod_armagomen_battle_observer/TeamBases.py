# -*- coding: utf-8 -*-
from gui.app_loader.loader import g_appLoader
from gui.Scaleform.framework import ViewTypes
from helpers import time_utils
from gui.Scaleform.daapi.view.battle.classic.team_bases_panel import TeamBasesPanel as TBP, _getSettingItem
from gui.battle_control.controllers.team_bases_ctrl import BattleTeamsBasesController
from PlayerEvents import g_playerEvents
from Core import sessionProvider
from Config import cfg
from BoEvents import eve

BASES = []

class TeamBases(object):
    __slots__ = ('basesDict', )
    def __init__(self):
        self.basesDict = {}
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle

    def battleLoading(self, arenaDP, player, teams, isSPG):
        app = g_appLoader.getDefBattleApp()
        if app:
            basesPanel = app.containerManager.getContainer(ViewTypes.VIEW).getView().components.get('teamBasesPanel')
            if basesPanel:
                if cfg.team_bases_panel['enabled'] and cfg.team_bases_panel['boBases'] and eve.componentPy:
                    basesPanel.flashObject.visible = False
                    BASES.extend([TBP.addCapturingTeamBase, TBP.updateTeamBasePoints, TBP.setTeamBaseCaptured, TBP.removeTeamBase, BattleTeamsBasesController.removeTeamsBases])
                    TBP.addCapturingTeamBase = lambda b_Self, cID, pTeam, points, x, tLeft, invCnt, stopped: self._addCapt(b_Self, cID, pTeam, points, x, tLeft, invCnt, stopped, eve.componentPy)
                    TBP.updateTeamBasePoints = lambda b_Self, cID, points, rate, tLeft, invCnt: self._update(b_Self, cID, points, rate, tLeft, invCnt, eve.componentPy)
                    TBP.setTeamBaseCaptured = lambda b_Self, cID, pTeam: self._setCaptured(b_Self, cID, pTeam, eve.componentPy)
                    TBP.removeTeamBase = lambda b_Self, cID: self._removeBase(b_Self, cID, eve.componentPy)
                    BattleTeamsBasesController.removeTeamsBases = lambda b_Self: self._remBaseS(b_Self, eve.componentPy)
                else:
                    basesPanel.flashObject.scaleX = basesPanel.flashObject.scaleY = basesPanel.flashObject.scaleZ = cfg.team_bases_panel['scale']
                    basesPanel.flashObject.y += cfg.team_bases_panel['y']

    def destroyBattle(self):
        if cfg.team_bases_panel['enabled'] and cfg.team_bases_panel['boBases'] and BASES:
            TBP.addCapturingTeamBase = BASES.pop(0)
            TBP.updateTeamBasePoints = BASES.pop(0)
            TBP.setTeamBaseCaptured = BASES.pop(0)
            TBP.removeTeamBase = BASES.pop(0)
            BattleTeamsBasesController.removeTeamsBases = BASES.pop(0)
            self.basesDict.clear()

    def _addCapt(self, b_Self, cID, pTeam, points, x, tLeft, invCnt, stopped, flashObject):
        BASES[0](b_Self, cID, pTeam, points, x, tLeft, invCnt, stopped)
        item = _getSettingItem(cID, pTeam, sessionProvider.arenaVisitor.type.getID())
        color = item.getColor()
        flashObject.as_updateBasesS(color, points / 100.0, invCnt, str(time_utils.getTimeLeftFormat(tLeft)))
        flashObject.as_updateBaseTextS(color, item.getCapturingString(points))
        flashObject.as_setBaseVisibleS(color, True)
        self.basesDict[cID] = item
        if stopped:
            self.stopCapture(cID, points, invCnt, flashObject)

    def _update(self, b_Self, cID, points, rate, tLeft, invCnt, flashObject):
        BASES[1](b_Self, cID, points, rate, tLeft, invCnt)
        item = self.basesDict.get(cID, None)
        if item:
            color = item.getColor()
            flashObject.as_updateBasesS(color, points / 100.0, invCnt, str(time_utils.getTimeLeftFormat(tLeft)))
            flashObject.as_updateBaseTextS(color, item.getCapturingString(points))

    def _setCaptured(self, b_Self, cID, pTeam, flashObject):
        BASES[2](b_Self, cID, pTeam)
        item = self.basesDict.get(cID, None)
        if item:
            flashObject.as_updateBaseTextS(item.getColor(), item.getCapturedString())

    def stopCapture(self, cID, points, invCnt, flashObject):
        item = self.basesDict.get(cID, None)
        if item:
            flashObject.as_updateBasesS(item.getColor(), points / 100.0, invCnt, '-')

    def _removeBase(self, b_Self, cID, flashObject):
        BASES[3](b_Self, cID)
        item = self.basesDict.pop(cID, None)
        if item:
            flashObject.as_setBaseVisibleS(item.getColor(), False)

    def _remBaseS(self, b_Self, flashObject):
        BASES[4](b_Self)
        for item in self.basesDict.values():
            flashObject.as_setBaseVisibleS(item.getColor(), False)
        self.basesDict.clear()