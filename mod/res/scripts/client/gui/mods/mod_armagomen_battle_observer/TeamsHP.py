# -*- coding: utf-8 -*-
import math
import BigWorld
from functools import partial
from account_helpers.settings_core.settings_constants import GRAPHICS
from Core import developer, mod_name, mod_version, sessionProvider, g_settingsCore, healthDic, vehiclesDic, totalsDict
from Config import cfg
from BoEvents import eve
from BoConstants import VEHICLE

class TeamsHP(object):
    __slots__ = ('allyTeam', 'enemyTeam', 'playerID', 'enabled', 'teams')
    
    def __init__(self):
        for attr in self.__slots__:
            setattr(self, attr, None)
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle

    def battleLoading(self, arenaDP, player, teams, isSPG):
        self.enabled = cfg.hp_bars["enabled"]
        self.playerID = player.playerVehicleID
        self.allyTeam, self.enemyTeam = teams
        self.teams = {self.allyTeam:"green", self.enemyTeam: "red"}
        player.onVehicleEnterWorld += self.__onEnterWorld
        player.arena.onVehicleKilled += self.__onVehicleKilled
        player.arena.onVehicleAdded += self.__onVehicleAddUpdate
        player.arena.onVehicleUpdated += self.__onVehicleAddUpdate
        eve.onHealthChanged += self.__onHealthChanged
        g_settingsCore.onSettingsApplied += self.__onSettingsApplied
        self.resyncHealth()

    def destroyBattle(self):
        player = BigWorld.player()
        player.onVehicleEnterWorld -= self.__onEnterWorld
        player.arena.onVehicleKilled -= self.__onVehicleKilled
        player.arena.onVehicleAdded -= self.__onVehicleAddUpdate
        player.arena.onVehicleUpdated -= self.__onVehicleAddUpdate
        eve.onHealthChanged -= self.__onHealthChanged
        g_settingsCore.onSettingsApplied -= self.__onSettingsApplied
        self.teams.clear()

    def resyncHealth(self):
        if self.enabled:
            for team in healthDic:
                if healthDic[team][VEHICLE.MAX]:
                    bar = math.ceil(healthDic[team][VEHICLE.CUR] * 100.0 / healthDic[team][VEHICLE.MAX]) / 100
                else:
                    bar = 1.0
                eve.componentPy.as_updateHealthS(self.teams[team], bar, healthDic[team][VEHICLE.CUR])
            self.updateDiff()

    def updateHealthPoints(self, team):
        health = healthDic[team][VEHICLE.CUR]
        maxHealts = healthDic[team][VEHICLE.MAX]
        if self.enabled:
            if maxHealts:
                bar = math.ceil(healthDic[team][VEHICLE.CUR] * 100.0 / maxHealts) / 100
            else:
                bar = 1.0
            eve.componentPy.as_updateHealthS(self.teams[team], bar, health)
            self.updateDiff()
        if cfg.main_gun['enabled'] and cfg.main_gun['mainGunDynamic'] and team == self.enemyTeam:
            if health and totalsDict[4] and totalsDict[1] and health < totalsDict[1]:
                totalsDict[4] = False
                eve.onMainGunFailed(True)

    def updateDiff(self):
        if cfg.hp_bars['differenceHP']:
            eve.componentPy.as_differenceS(healthDic[self.allyTeam][VEHICLE.CUR] - healthDic[self.enemyTeam][VEHICLE.CUR])

    def __onSettingsApplied(self, diff):
        if GRAPHICS.COLOR_BLIND in diff:
            eve.componentPy.as_colorBlindS(cfg.colors['hpBar']['enemyHpBarColor%s' % ('Blind' if diff[GRAPHICS.COLOR_BLIND] else '')].replace('#', '0x'))
            self.resyncHealth()

    def __onHealthChanged(self, targetID, targetTeam, newHealth, attackerID, aRID):
        target = vehiclesDic.get(targetID)
        if target and target[VEHICLE.CUR] > 0:
            damage = target[VEHICLE.CUR] - newHealth
            target[VEHICLE.CUR] = newHealth
            healthDic[targetTeam][VEHICLE.CUR] -= damage
            self.updateHealthPoints(targetTeam)
            if newHealth:
                eve.drawGraphics(targetID, target)
            attacker = vehiclesDic.get(attackerID)
            if attacker:
                BigWorld.callback(0.4, partial(eve.onPlayersDamaged, attackerID, damage, targetTeam, attacker[VEHICLE.TEAM], aRID))

    def __onVehicleKilled(self, targetID, attackerID, equipmentID, reason):
        BigWorld.callback(1, partial(self.__updateKills, targetID, attackerID))

    def __updateKills(self, targetID, attackerID):
        target = vehiclesDic.get(targetID)
        if target and target[VEHICLE.CUR] > 0:
            healthDic[target[VEHICLE.TEAM]][VEHICLE.CUR] -= target[VEHICLE.CUR]
            self.updateHealthPoints(target[VEHICLE.TEAM])
            target[VEHICLE.CUR] = 0
        if self.playerID == targetID:
            eve.onPlayerVehicleDeath(attackerID)

    def __onVehicleAddUpdate(self, tid):
        if tid in vehiclesDic:
            return
        vInfoVO = sessionProvider.getArenaDP().getVehicleInfo(tid)
        if vInfoVO:
            vehicleType = vInfoVO.vehicleType
            if vehicleType:
                maxHealth = vehicleType.maxHealth
                classTag = vehicleType.classTag
                if maxHealth and vehicleType and classTag:
                    health = maxHealth if vInfoVO.isAlive() else 0
                    vehiclesDic[tid] = [health, maxHealth, vInfoVO.team, classTag]
                    hDic = healthDic.get(vInfoVO.team)
                    hDic[VEHICLE.CUR] += health
                    hDic[VEHICLE.MAX] += maxHealth
                    self.resyncHealth()
                    eve.componentPy.as_AddVehIdToListS(tid)
                    eve.onVehicleAddUpdate(tid, vehicleType)
                    #BigWorld.callback(0.4, partial(eve.onVehicleAddUpdate, tid, vehicleType))

    def __onEnterWorld(self, vehicle):
        target = vehiclesDic.get(vehicle.id)
        newHealth = max(0, vehicle.health)
        if target and target[VEHICLE.CUR] != newHealth and vehicle.isAlive():
            diff = target[VEHICLE.CUR] - newHealth
            healthDic[target[VEHICLE.TEAM]][VEHICLE.CUR] -= diff
            self.updateHealthPoints(target[VEHICLE.TEAM])
            target[VEHICLE.CUR] = newHealth
            eve.drawGraphics(vehicle.id, target)