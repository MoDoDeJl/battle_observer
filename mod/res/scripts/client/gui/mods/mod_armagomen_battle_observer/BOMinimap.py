# -*- coding: utf-8 -*-
from BoEvents import eve
from Config import cfg
from account_helpers.settings_core import settings_constants
from Core import g_settingsCore
from gui.app_loader.loader import g_appLoader

class BOMinimap(object):
    __slots__ = ()
    def __init__(self):
        eve.battleLoading += self._battleLoading
        eve.destroyBattle += self._destroyBattle
        
    def _battleLoading(self, arenaDP, player, teams, isSPG):
        if cfg.minimap["enabled"]:
            g_appLoader.getDefBattleApp().graphicsOptimizationManager.switchOptimizationEnabled(False)
            eve.onKeyPressed += self.keyEvent
        
    def _destroyBattle(self):
        if cfg.minimap["enabled"]:
            g_appLoader.getDefBattleApp().graphicsOptimizationManager.switchOptimizationEnabled(True)
            eve.onKeyPressed -= self.keyEvent
        
    def keyEvent(self, key, isKeyDown):
        if key in cfg.minimap["key"]:
            eve.componentPy.as_MinimapCenteredS(isKeyDown)