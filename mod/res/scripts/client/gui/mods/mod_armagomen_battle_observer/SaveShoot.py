# -*- coding: utf-8 -*-
import BigWorld
import Vehicle
from Avatar import PlayerAvatar
from messenger import MessengerEntry
from Config import cfg
from BoEvents import eve
from Core import sessionProvider

SHOOT = []

class SaveShootLite(object):
    __slots__ = ('msg', 'aliveOnly', 'playerTeam')
    def __init__(self):
        for attr in self.__slots__:
            setattr(self, attr, None)
        eve.battleLoading += self._battleLoading
        eve.destroyBattle += self._destroyBattle

    def _battleLoading(self, arenaDP, player, teams, isSPG):
        if cfg.save_shoot['enabled']:
            SHOOT.append(PlayerAvatar.shoot)
            PlayerAvatar.shoot = lambda *args, **kwargs: self.newShoot(*args, **kwargs)
            self.aliveOnly = cfg.save_shoot['aliveOnly']
            self.msg = cfg.save_shoot['msg']
            self.playerTeam = teams[0]

    def _destroyBattle(self):
        if cfg.save_shoot['enabled'] and SHOOT:
            PlayerAvatar.shoot = SHOOT.pop(0)

    def newShoot(self, *args, **kwargs):
        target = BigWorld.target()
        if target is not None and isinstance(target, Vehicle.Vehicle):
            if self.isAliveCheck(target) or self.teamCheck(target):
                return
        SHOOT[0](*args, **kwargs)

    def isAliveCheck(self, target):
        return not target.isAlive() and self.aliveOnly

    def teamCheck(self, target):
        isAlly = target.publicInfo.team == self.playerTeam
        if isAlly:
            MessengerEntry.g_instance.gui.addClientMessage(self.msg)
        return isAlly