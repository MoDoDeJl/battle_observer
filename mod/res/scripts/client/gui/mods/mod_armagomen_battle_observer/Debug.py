# -*- coding: utf-8 -*-
import BigWorld
from gui.Scaleform.daapi.view.battle.shared.debug_panel import DebugPanel
from Config import cfg
from BoEvents import eve
from BoConstants import DEBUG

BASE_DEBUG = []

class Debug(object):
    __slots__ = ('template', 'colors')
    def __init__(self):
        for attr in self.__slots__:
            setattr(self, attr, None)
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle

    def __debug(self, ping, fps, isLaggingNow, fpsReplay):
        eve.componentPy.up_fpsPingS(self.template.replace("{{FPS}}", "%d" % fps).replace("{{PingLagColor}}", self.colors[int(isLaggingNow)]).replace("{{PING}}", "%d" % ping))

    def battleLoading(self, arenaDP, player, teams, isSPG):
        if cfg.debugPanel['enabled']:
            self.template = cfg.debugPanel['debugTemplate']
            self.colors = (cfg.colors['debug']['pingColor'], cfg.colors['debug']['pingLagColor'])
            BASE_DEBUG.append(DebugPanel.updateDebugInfo)
            DebugPanel.updateDebugInfo = lambda base_self, ping, fps, isLaggingNow, fpsReplay: self.__debug(ping, fps, isLaggingNow, fpsReplay)
        
    def destroyBattle(self):
        if cfg.debugPanel['enabled']:
            DebugPanel.updateDebugInfo = BASE_DEBUG.pop(0)