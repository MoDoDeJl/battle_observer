# -*- coding: utf-8 -*-
import os
import codecs
import json
import time
from Core import DEFAULT_LANGUAGE

class Config(object):
    __slots__ = ('main', 'debugPanel', 'battleTimer', 'zoom', 'arcadeCamera', 'strategicCamera', 'armor_calculator', 'colors', 'log_global',
                 'log_total', 'log_damage_extended', 'log_input_extended', 'hp_bars', 'team_bases_panel', 'markers', 'main_gun', 'vehicle_types',
                 'players_spotted', 'players_damages', 'players_bars', 'flight_time', 'save_shoot', 'minimap')
    def __init__(self):
        self.main = {
            "PYTHON_LOG_ANTISPAM": True,
            "CHECK_UPDATES": True,
            "hideChatInRandom": False,
            "background": False,
            "user_background": [0, 0, None],
            "backgroundTransparency": 0.25,
            "hideBadges": False
        }
        self.debugPanel = {
            "enabled": False,
            "position_x": 5,
            "position_y": 0,
            "scale": 1,
            "debugTemplate": "<textformat tabstops='[75]'>FPS <font color='#E0E06D'><b>{{FPS}}</b></font><tab>PING <font color='{{PingLagColor}}'><b>{{PING}}</b></font></textformat>"
        }
        self.battleTimer = {"enabled": False, "clockFormat": "%d %B %H:%M", "clockTemplate": "{{time}}", "timerTemplate": "<font color='{{timerColor}}'>{{timer}}</font>"}
        self.zoom = {
            "enabled": False,
            "LENS_EFFECTS_ENABLED": False,
            "dynamyc_zoom": {"enable": False, "zoomToGunMarker": False, "maxZoom": 30.0, "minZoom": 2.0, "zoomXMetrs": 17},
            "default_zoom": {"enable": False, "defZoom": 4.0},
            "zoomSteps": [2.0, 4.0, 8.0, 12.0, 16.0, 20.0, 25.0, 30.0]
            }
        self.arcadeCamera = {"enabled": False, "min": 4.0, "max": 80.0, "startDeadDist": 20.0}
        self.strategicCamera = {"enabled": False, "min": 40.0, "max": 150.0}
        self.armor_calculator = {
            "enabled": False,
            "showCalcPoints": False,
            "showText": False,
            "calcPosition": {"x": 0, "y": 100},
            "calcTextPosition":{"x": 0, "y": 120},
            "template": "<font color='{{color}}'>{{calcedArmor}}</font>",
            "armorCalculatorText": {"green": "100%", "orange": "50%", "red": "0%", "yellow": "50%", "purple": "0%"}
        }
        self.colors = {
            "mainGun": {"mainGunColor": "#E0E06D"},
            "battleTimer": {
                "timerColor": "#E0E06D",
                "timerColorEndBattle": "#F30900"
                },
            "debug":{
                "pingColor": "#E0E06D",
                "pingLagColor": "#F30900"
            },
            "hpBar": {
                "alliesHpBarColor": "#5ACB00",
                "enemyHpBarColor": "#F30900",
                "enemyHpBarColorBlind": "#6F6CD3",
                "backgroundHpBarColor": "#000000",
                "bg_bars_alpha": 0.6,
                "hpBarsTransparency": 0.85
            },
            "markers": {"ally": "#5ACB00", "enemy": "#F30900", "enemyColorBlind": "#6F6CD3", "deadColor": "#858585"},
            "colorAvgDmg": {"very_bad": "#FF2600", "bad": "#FF8B00", "normal": "#FFC900", "good": "#98EA00", "very_good": "#02D0CE", "unique": "#D04AF3"},
            "attackerColor": {"isSquadMan": "#FF9200", "isTeamDamage": "#5ACB00", "isEnemy": "#F30900", "unknown": "#FAFAFA"},
            "armorCalculatorColor": {"green": "#66FF33", "orange": "#FF9900", "red": "#FF0000", "yellow": "#FFC900", "purple": "#6F6CD3"}
        }
        self.log_global = {
            "wg_log_pos_fix": False,
            "wg_log_hide_crits": False,
            "wg_log_hide_block": False,
            "wg_log_hide_assist": False,
            "logsAltmodeKey": [56, 184],
            "attackReason": {
                "shot": "<img src='img://gui/maps/icons/library/efficiency/48x48/damage.png' width='24' height='24' vspace='-13'>",
                "fire": "<img src='img://gui/maps/icons/library/efficiency/48x48/fire.png' width='24' height='24' vspace='-13'>",
                "ramming": "<img src='img://gui/maps/icons/library/efficiency/48x48/ram.png' width='24' height='24' vspace='-13'>",
                "world_collision": "<img src='img://gui/maps/icons/library/efficiency/48x48/ram.png' width='24' height='24' vspace='-13'>",
                "death_zone": "<img src='img://gui/maps/icons/library/efficiency/48x48/module.png' width='24' height='24' vspace='-13'>",
                "drowning": "<img src='img://gui/maps/icons/library/efficiency/48x48/module.png' width='24' height='24' vspace='-13'>",
                "gas_attack": "<img src='img://gui/maps/icons/library/efficiency/48x48/module.png' width='24' height='24' vspace='-13'>",
                "overturn": "<img src='img://gui/maps/icons/library/efficiency/48x48/module.png' width='24' height='24' vspace='-13'>",
                "bomber_eq": "<img src='img://gui/maps/icons/library/efficiency/48x48/module.png' width='24' height='24' vspace='-13'>",
                "artillery_eq": "<img src='img://gui/maps/icons/library/efficiency/48x48/module.png' width='24' height='24' vspace='-13'>"
            }
        }
        self.log_total = {
            "enabled": False,
            "logPosition_x": 180,
            "logPosition_y": 0,
            "templateMainDMG":"<textformat leading='-3'>{{damageIcon}}<font color='{{tankDamageAvgColor}}'>{{playerDamage}}</font>{{blockedIcon}}{{blockedDamage}}{{assistIcon}}{{assistDamage}}{{spottedIcon}}{{spottedTanks}}{{stunIcon}}{{stun}}{{mainGun}}</textformat>",
            "damageIcon": " <img src='img://gui/maps/icons/library/efficiency/48x48/damage.png' width='24' height='24' vspace='-10'>",
            "blockedIcon": " <img src='img://gui/maps/icons/library/efficiency/48x48/armor.png' width='24' height='24' vspace='-9'>",
            "assistIcon": " <img src='img://gui/maps/icons/library/efficiency/48x48/help.png' width='24' height='24' vspace='-10'>",
            "spottedIcon": " <img src='img://gui/maps/icons/library/efficiency/48x48/detection.png' width='24' height='24' vspace='-10'>",
            "stunIcon": " <img src='img://gui/maps/icons/library/efficiency/48x48/stun.png' width='24' height='24' vspace='-10'>",
            "mainLogScale" : 1
        }
        self.log_damage_extended = {
            "enabled": False,
            "damageLogPosition": {"x":300, "y":60},
            "reverse": False,
            "killedIcon": "<img src='img://gui/maps/icons/library/efficiency/48x48/destruction.png' width='24' height='24' vspace='-13'>",
            "startTag": "<textformat leading='-7' tabstops='[20, 55, 80, 100]'><font size='15'>",
            "extendedLog":"<font size='12'>{{index}}:</font><tab><font color='#E0E06D'>{{totalDamage}}</font><tab>{{attackReason}}<tab><font color='{{tankClassColor}}'>{{classIcon}}</font><tab>{{tankName}}{{killedIcon}}",
            "extendedLogALTMODE":"<font size='12'>{{shots}}:</font><tab><font color='#E0E06D'>{{lastDamage}}</font><tab>{{attackReason}}<tab><font color='{{tankClassColor}}'>{{classIcon}}</font><tab>{{userName}}{{killedIcon}}",
            "endTag": "</font></textformat>",
            "shellTypes": {"ARMOR_PIERCING_CR":"БП", "ARMOR_PIERCING":"ББ", "ARMOR_PIERCING_HE":"ББ", "HIGH_EXPLOSIVE":"ОФ", "HOLLOW_CHARGE":"КС", "UNDEFINED":"--"},
            "shellColor": {"normal":"#FAFAFA", "gold":"#FFD700"}
            }
        self.log_input_extended = {
            "enabled": False,
            "reverse": False,
            "inputLogPosition":{"x":235, "y":235},
            "startTag": "<textformat leading='-7' tabstops='[20, 55, 80, 100, 125]'><font size='15'>",
            "extendedLog":"<font size='12'>{{index}}:</font><tab><font color='#E0E06D'>{{totalDamage}}</font><tab><font color='{{shellColor}}'>{{shellType}}</font><tab>{{attackReason}}<tab><font color='{{tankClassColor}}'>{{classIcon}}</font><tab><font color='{{attackerColor}}'>{{tankName}}</font>{{killedIcon}}",
            "extendedLogALTMODE":"<font size='12'>{{index}}:</font><tab><font color='#E0E06D'>{{lastDamage}}</font><tab><font color='{{shellColor}}'>{{shellType}}</font><tab>{{attackReason}}<tab><font color='{{tankClassColor}}'>{{classIcon}}</font><tab><font color='{{attackerColor}}'>{{userName}}</font>{{killedIcon}}",
            "endTag": "</font></textformat>",
            "killedIcon": "<img src='img://gui/maps/icons/library/efficiency/48x48/destruction.png' width='24' height='24' vspace='-13'>",
            "shellTypes": {"ARMOR_PIERCING_CR":"БП", "ARMOR_PIERCING":"ББ", "ARMOR_PIERCING_HE":"ББ", "HIGH_EXPLOSIVE":"ОФ", "HOLLOW_CHARGE":"КС", "UNDEFINED":"--"},
            "shellColor": {"normal":"#FAFAFA", "gold":"#FFD700"}
        }
        self.hp_bars = {"enabled": True, "style": "legue", "barsWidth": 200, "differenceHP": True, "showAliveCount": False}
        self.team_bases_panel = {"enabled": True, "y": 100, "scale": 1, "boBases":False}
        self.markers = {"showMarkers_KEY": [82], "markersClassColor": False, "position": {"ally_x": {"normal": -45, "legue": -10}, "ally_y": {"normal": 31, "legue": 60}, "enemy_x": {"normal": 45, "legue": 10}, "enemy_y": {"normal": 31, "legue": 60}}}
        self.main_gun = {
            "enabled": False,
            "mainGunDynamic": True,
            "template": " {{mainGunIcon}}{{mainGunDoneIcon}}{{mainGunFailureIcon}}<font color='{{mainGunColor}}'>{{mainGun}}</font>",
            "position_x": 255,
            "position_y": 0,
            "mainGunIcon": "<img src='img://gui/maps/icons/achievement/32x32/mainGun.png' width='26' height='25' vspace='-7'>",
            "mainGunDoneIcon": "<img src='img://gui/maps/icons/library/done.png' width='24' height='24' vspace='-8'>",
            "mainGunFailureIcon":"<img src='img://gui/maps/icons/library/icon_alert_32x32.png' width='22' height='22' vspace='-6'>"
        }
        self.vehicle_types = {
            "vehicleClassColors": {"heavyTank": "#FF9933", "mediumTank": "#FFCC00", "AT-SPG": "#3399CC", "SPG": "#FF3300", "lightTank": "#66FF00", "unknown":"#FAFAFA"},
            "vehicleClassIcon": {
                "AT-SPG":"<font face='BattleObserver' size='20'>J</font>",
                "SPG":"<font face='BattleObserver' size='20'>S</font>",
                "heavyTank":"<font face='BattleObserver' size='20'>H</font>",
                "lightTank":"<font face='BattleObserver' size='20'>L</font>",
                "mediumTank":"<font face='BattleObserver' size='20'>M</font>",
                "unknown":"<font face='BattleObserver' size='20'>U</font>"
            },
            "vehicleClassColorsOnPanels": False
        }
        self.players_spotted = {
            "enabled": False,
            "status": {"lights": "<font face='$TitleFont' color='#00FF00' size='24'>*</font>", "donotlight": "<font face='$TitleFont' color='#FF0000' size='24'>*</font>"},
            "settings": {"x": -40, "y": -2, "align": "center"}
        }
        self.players_damages = {
            "enabled": False,
            "damages_key": [56, 184],
            "damages_text": "<font color='#FFFF00'>{{damage}}</font>",
            "damages_settings": {"x": -50, "y": -2, "align": "left"}
        }
        self.players_bars = {
            "enabled": False,
            "hpbarsclassColor": False,
            "hpbarsShowKey": [56, 184],
            "showHpBarsOnKeyDown": False,
            "hp_text": "<font face='$TitleFont' color='#FAFAFA' size='15'>{{health}}</font>",
            "hp_bar_settings": {"bars_x": 80, "bars_y": 2, "bars_width": 60, "bars_height": 20}
        }
        self.flight_time = {
            "enabled":False,
            "pos_x":0,
            "pos_y":-250,
            "spgOnly":True,
            "template":"<font color='#FFFFFF'>{{flightTime}}</font>"
        }
        self.save_shoot = {
            "enabled": False,
            "aliveOnly": True,
            "msg": "Shooting on allies, locked."
        }
        self.minimap = {
            "enabled": False,
            "key": [29, 157]
        }
cfg = Config()

class ConfigLoader(object):
    __slots__ = ('cName', 'path', 'configsList', 'load_list')
    def __init__(self):
        self.cName = None
        self.path = "./mods/configs/mod_battle_observer/"
        self.configsList = [x for x in os.listdir(self.path) if os.path.isdir(os.path.join(self.path, x))]
        self.load_list = ('hp_bars.json', 'main.json', 'main_gun.json', 'markers.json', 'debugPanel.json', 'battleTimer.json',
         'vehicle_types.json', 'zoom.json', 'colors.json', 'armor_calculator.json', 'team_bases_panel.json', 'flight_time.json',
         'arcadeCamera.json', 'strategicCamera.json', 'players_spotted.json', 'players_damages.json', 'players_bars.json',
         'log_global.json', 'log_total.json', 'log_damage_extended.json', 'log_input_extended.json', 'save_shoot.json', 'minimap.json')
        self.getConfig(self.path)

    def getFileData(self, path):
        with codecs.open(path, 'r', 'utf-8-sig') as fh:
            loader = fh.read().encode('utf-8')
            temp_line_data = []
            for line in loader.split('\n'):
                if '//' in line:
                    for idx, char in enumerate(line):
                        if char == "/" and line[idx + 1] == "/" and line[idx - 1] != ':':
                            line = line[:idx]
                            break
                line = line.strip()
                if line:
                    temp_line_data.append(line)
            return json.loads(''.join(temp_line_data))

    def loadError(self, file_name, error):
        topic = 'ОШИБКА ФАЙЛA КОНФИГА' if DEFAULT_LANGUAGE == "ru" else 'ERROR CONFIG DATA'
        with codecs.open('./mods/configs/mod_battle_observer/Errors.log', 'a', 'utf-8-sig') as fh:
            fh.write('%s: %s: %s, %s\n' % (time.asctime(), topic, file_name, error))

    def getConfig(self, path):
        loadJSON = path + "load.json"
        if os.path.exists(path):
            if os.path.exists(loadJSON):
                self.cName = self.getFileData(loadJSON).get('loadConfig')
                if not os.path.exists(path + self.cName):
                    os.makedirs(path + self.cName)
            else:
                self.cName = self.createLoadJSON(loadJSON)
                if not os.path.exists(path + "default_Minimal"):
                    os.makedirs(path + "default_Minimal")
        else:
            self.loadError(loadJSON, 'CONFIGURATION FILES IS NOT FOUND')
            os.makedirs(path + "default_Minimal")
            self.cName = self.createLoadJSON(loadJSON)
        self.readConfig(self.cName)
        if not self.configsList:
            self.configsList.append(self.cName)

    def createLoadJSON(self, path):
        cName = "default_Minimal"
        self.createFileInDir(path, {"loadConfig": cName})
        self.loadError(path, 'NEW CONFIGURATION FILE load.json IS CREATED')
        return cName

    def createFileInDir(self, path, data):
        with codecs.open(path, 'w+', 'utf-8-sig') as fh:
            fh.write(json.dumps(data, ensure_ascii=False, indent=2, separators=(',', ':'), sort_keys=True))

    def encodeToUtf(self, obj):
        if isinstance(obj, unicode):
            return obj.encode('utf-8').replace('\t', '<tab>').replace('\n', '<br>').replace('\r', '<br>')
        elif isinstance(obj, list) and obj:
            return [x.encode('utf-8').replace('\t', '<tab>').replace('\n', '<br>').replace('\r', '<br>') if isinstance(x, unicode) else x for x in obj]
        else:
            return obj

    def getValue(self, data, key, value):
        param = data.get(key, None)
        if param is not None:
            return (self.encodeToUtf(param), False)
        else:
            return (value, True)

    def updateData(self, data, update_conf):
        fileUpdate = False
        for key, value in update_conf.iteritems():
            param, upd = self.getValue(data, key, value)
            fileUpdate |= upd
            if not isinstance(value, dict):
                update_conf[key] = param
            else:
                fileUpdate |= self.updateData(data[key], update_conf[key])
        return fileUpdate

    def readConfig(self, name):
        direct = '%s%s' % (self.path, name)
        print '[BATTLE_OBSERVER_INFO] START UPDATE USER CONFIGURATION: %s' % name
        filedir = os.listdir(direct)
        for fileName in self.load_list:
            path = "%s/%s" % (direct, fileName)
            update_conf = getattr(cfg, fileName.replace('.json', ''))
            if fileName in filedir:
                try:
                    if self.updateData(self.getFileData(path), update_conf):
                        self.createFileInDir(path, update_conf)
                except Exception as error:
                    self.loadError(path, error)
                    continue
            else:
                self.createFileInDir(path, update_conf)
        print '[BATTLE_OBSERVER_INFO] CONFIGURATION UPDATE COMPLETED: %s' % name

c_Loader = ConfigLoader()