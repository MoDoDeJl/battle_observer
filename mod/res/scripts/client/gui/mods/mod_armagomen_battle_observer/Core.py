# -*- coding: utf-8 -*-
import BigWorld
from helpers import getClientLanguage, dependency
from skeletons.connection_mgr import IConnectionManager
from skeletons.account_helpers.settings_core import ISettingsCore
from skeletons.gui.battle_session import IBattleSessionProvider
from skeletons.gui.shared import IItemsCache

mod_name = 'Battle Observer'
developer = 'Armagomen'
local_ver = '1.15.3'
mod_version = '%s - %s' % (local_ver, BigWorld.currentModPathBO)
DEFAULT_LANGUAGE = getClientLanguage().lower()

connectionManager = dependency.instance(IConnectionManager)
sessionProvider = dependency.instance(IBattleSessionProvider)
g_settingsCore = dependency.instance(ISettingsCore)
g_itemsCache = dependency.instance(IItemsCache)

playersDamage = {}
healthDic = {}
vehiclesDic = {}
totalsDict = {0: 0, 1: 0, 2: 0, 3: "", 4: True, 5: 0}
topLogDict = {}

def macrosReplace(data, params):
    for key, value in params.iteritems():
        data = data.replace(key, value)
    return data
    
def modMessage(data):
    print '[BATTLE_OBSERVER_INFO] MOD %s: %s by %s %s' % (data, mod_name, developer, mod_version)

modMessage('START LOADING')
def fini():
    modMessage('SHUTTING DOWN')