# -*- coding: utf-8 -*-
from AvatarInputHandler.aih_constants import SHOT_RESULT
from gui.Scaleform.daapi.view.battle.shared.crosshair.plugins import ShotResultIndicatorPlugin
from Core import macrosReplace
from Config import cfg
from BoEvents import eve
from BoConstants import ARMOR_CALC

NONEDATA = (None, None, SHOT_RESULT.UNDEFINED, None)
BASE_CALC = []

class ArmorCalculator(object):
    __slots__ = ('typeColors', 'template', 'text', 'showCalcPoints', 'showText', 'isSPG', 'player', 'playerTeam', 'calcCache')
    def __init__(self):
        for attr in self.__slots__:
            setattr(self, attr, None)
        eve.battleLoading += self._battleLoading
        eve.destroyBattle += self._destroyBattle

    def _battleLoading(self, arenaDP, player, teams, isSPG):
        self.isSPG = isSPG
        if cfg.armor_calculator['enabled'] and not isSPG:
            self.player = player
            self.typeColors = cfg.colors['armorCalculatorColor']
            self.template = cfg.armor_calculator['template']
            self.text = cfg.armor_calculator['armorCalculatorText']
            self.showCalcPoints = cfg.armor_calculator['showCalcPoints']
            self.showText = cfg.armor_calculator['showText']
            self.playerTeam = teams[0]
            eve.onPlayerVehicleDeath += self.onPlayerVehicleDeath
            BASE_CALC.append(ShotResultIndicatorPlugin._ShotResultIndicatorPlugin__updateColor)
            ShotResultIndicatorPlugin._ShotResultIndicatorPlugin__updateColor = lambda b_self, mType, targetPos, collision, direction: self.__updateColor(b_self, mType, targetPos, collision, direction)

    def _destroyBattle(self):
        if cfg.armor_calculator['enabled'] and not self.isSPG and BASE_CALC:
            ShotResultIndicatorPlugin._ShotResultIndicatorPlugin__updateColor = BASE_CALC.pop(0)
            eve.onPlayerVehicleDeath -= self.onPlayerVehicleDeath

    def onPlayerVehicleDeath(self, attackerID):
        self.clearView()

    def __updateColor(self, b_self, markerType, targetPos, collision, direction):
            armor_sum, calced_armor, result, shotParams = self.getCalcedArmor(collision, targetPos, direction)
            if result in b_self._ShotResultIndicatorPlugin__colors:
                color = b_self._ShotResultIndicatorPlugin__colors[result]
                colorChange = b_self._ShotResultIndicatorPlugin__cache[markerType] != result and b_self._parentObj.setGunMarkerColor(markerType, color)
                if colorChange:
                    b_self._ShotResultIndicatorPlugin__cache[markerType] = result
                    if calced_armor is None:
                        self.clearView()
                        return
                if result:
                    self.sendToFlash(color, shotParams, calced_armor, armor_sum, colorChange)

    def sendToFlash(self, color, shotParams, calced_armor, armor_sum, colorChange):
        if self.showText and colorChange:
            eve.componentPy.as_armorTextSS("<font color='%s'>%s</font>" % (self.typeColors[color], self.text[color]))
        if self.showCalcPoints and self.calcCache != calced_armor:
            self.calcCache = calced_armor
            if calced_armor:
                data = macrosReplace(self.template, {"{{color}}": self.typeColors[color],
                 "{{calcedArmor}}": "%d" % calced_armor,
                 "{{armor}}": "%d" % armor_sum,
                 "{{piercingPower}}": "%d" % shotParams.piercingPower[0],
                 "{{caliber}}": "%d" % shotParams.shell.caliber})
                eve.componentPy.as_armorCalcS(data)

    def clearView(self):
        eve.componentPy.as_armorTextSS("")
        eve.componentPy.as_armorCalcS("")
                
    def getCalcedArmor(self, collision, targetPos, direction):
        if collision and collision.isVehicle:
            entity = collision.entity
            if entity.publicInfo['team'] != self.playerTeam and entity.health > 0:
                collisionsDetails = self._getAllCollisionDetails(targetPos, direction, entity)
                if collisionsDetails is not None:
                    shotParams = self.player.getVehicleDescriptor().shot
                    armor = []
                    hitCos = 1.0
                    mid_dist = (collisionsDetails[0][0] + collisionsDetails[-1][0]) * 0.5
                    for dist, hitAngleCos, matInfo, compIdx in collisionsDetails:
                        if mid_dist > dist:
                            if matInfo and matInfo.armor not in armor:
                                armor.append(matInfo.armor)
                                if compIdx not in ARMOR_CALC.SKIP_DETAILS and 0.0 < hitAngleCos < 1.0 and hitCos == 1.0:
                                    hitCos = hitAngleCos
                    armor_sum = sum(armor, 0)
                    if armor_sum:
                        calced_armor = armor_sum / hitCos
                        result = self.__getShotResult(shotParams, calced_armor, targetPos)
                        return (armor_sum, calced_armor, result, shotParams)
                    else:
                        return NONEDATA
        return NONEDATA
    
    def __getShotResult(self, shotParams, calced_armor, targetPos):
        p100, p500 = shotParams.piercingPower
        piercingPower = p100
        if shotParams.shell.kind in ARMOR_CALC.SHELL_DIST_RATE:
            dist = (targetPos - self.player.getOwnVehiclePosition()).length
            if dist > ARMOR_CALC.MIN_DIST:
                piercingPower = max(p500, p100 + (p500 - p100) * (dist - ARMOR_CALC.MIN_DIST) / 400.0)
        if calced_armor < piercingPower * 0.75:
            return SHOT_RESULT.GREAT_PIERCED
        elif calced_armor > piercingPower * 1.25:
            return SHOT_RESULT.NOT_PIERCED
        else:
            return SHOT_RESULT.LITTLE_PIERCED

    def _getAllCollisionDetails(self, targetPos, direction, entity):
        startPoint = targetPos - direction * ARMOR_CALC.BACKWARD_LENGTH
        endPoint = targetPos + direction * ARMOR_CALC.FORWARD_LENGTH
        return entity.collideSegmentExt(startPoint, endPoint)