# -*- coding: utf-8 -*-
import math
import BigWorld
from functools import partial
from gui.Scaleform.daapi.view.battle.shared.minimap.plugins import ArenaVehiclesPlugin
from account_helpers.settings_core.settings_constants import GRAPHICS
from Core import macrosReplace, mod_name, sessionProvider, g_settingsCore, playersDamage, vehiclesDic
from Config import cfg
from BoEvents import eve
from BoConstants import VEHICLE, EPIC_BATTLE, TEAM

SPOTSTAT = {}
COLORS = {}
BASE_SETINAOI = []

class PlayersPanels(object):
    __slots__ = ('isEnemy', 'bar_x', 'bars_y', 'bars_width', 'bars_height', 'hp_text', 'vClassColors', 'hp_on_alt', 'hpBarsEnable')
    def __init__(self):
        for attr in self.__slots__:
            setattr(self, attr, None)
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle

    def updateParams(self):
        settings = cfg.players_bars['hp_bar_settings']
        self.bar_x = settings['bars_x']
        self.bars_y = settings['bars_y']
        self.bars_width = settings['bars_width']
        self.bars_height = settings['bars_height']
        COLORS.update({False: int(cfg.colors['hpBar']['alliesHpBarColor'].replace('#', ''), 16), True: int(cfg.colors['hpBar']['enemyHpBarColor%s' % ('Blind' if g_settingsCore.getSetting(GRAPHICS.COLOR_BLIND) else '')].replace('#', ''), 16)})
        self.hp_text = cfg.players_bars['hp_text']
        self.vClassColors = cfg.vehicle_types['vehicleClassColors']
        self.hpBarsEnable = cfg.players_bars['enabled']
        self.hp_on_alt = cfg.players_bars['showHpBarsOnKeyDown']
        
    def battleLoading(self, arenaDP, player, teams, isSPG):
        if player.arena.bonusType in EPIC_BATTLE:
            return
        if cfg.players_spotted['enabled']:
            SPOTSTAT.update({False: cfg.players_spotted['status']['donotlight'], True: cfg.players_spotted['status']['lights']})
            BASE_SETINAOI.append(ArenaVehiclesPlugin._ArenaVehiclesPlugin__setInAoI)
            ArenaVehiclesPlugin._ArenaVehiclesPlugin__setInAoI = lambda base_self, entry, isInAoI: self.setInAoI(base_self, entry, isInAoI)
        self.updateParams()
        self.isEnemy = sessionProvider.getCtx().isEnemy
        eve.drawGraphics += self.drawGraphics
        player.arena.onVehicleKilled += self.onVehicleKilled
        eve.onKeyPressed += self.keyEvent
        g_settingsCore.onSettingsApplied += self.onSettingsApplied
        eve.onVehicleAddUpdate += self.onVehicleAdd
        self.updateLinks()
            
    def destroyBattle(self):
        player = BigWorld.player()
        if player.arena.bonusType in EPIC_BATTLE:
            return
        if cfg.players_spotted['enabled'] and BASE_SETINAOI:
            ArenaVehiclesPlugin._ArenaVehiclesPlugin__setInAoI = BASE_SETINAOI.pop(0)
        eve.drawGraphics -= self.drawGraphics
        player.arena.onVehicleKilled -= self.onVehicleKilled
        eve.onKeyPressed -= self.keyEvent
        g_settingsCore.onSettingsApplied -= self.onSettingsApplied
        eve.onVehicleAddUpdate -= self.onVehicleAdd
    
    def keyEvent(self, key, isKeyDown):
        if self.hp_on_alt and key in cfg.players_bars['hpbarsShowKey']:
            self.healthOnAlt(isKeyDown)

    def onVehicleKilled(self, targetID, attackerID, equipmentID, reason):
        eve.componentPy.as_updateTextFieldS(targetID, "InAoiTf", "")
        if self.hpBarsEnable:
            eve.componentPy.as_setHPbarsVisibleS(targetID, False)
            
    def onSettingsApplied(self, diff):
        if self.hpBarsEnable:
            if GRAPHICS.COLOR_BLIND in diff:
                if cfg.players_bars['hpbarsclassColor']:
                    return
                COLORS.update({False: int(cfg.colors['hpBar']['alliesHpBarColor'].replace('#', ''), 16), True: int(cfg.colors['hpBar']['enemyHpBarColor%s' % ('Blind' if diff[GRAPHICS.COLOR_BLIND] else '')].replace('#', ''), 16)})
                for vehicleID, vehicle in vehiclesDic.iteritems():
                    eve.componentPy.as_colorBlindPPbarsS(vehicleID, COLORS[self.isEnemy(vehicleID)])

    def setInAoI(self, base_self, entry, isInAoI):
        BASE_SETINAOI[0](base_self, entry, isInAoI)
        if entry._isEnemy and entry._isAlive:
            entryID = entry._entryID
            for vehicleID, vehicle in base_self._entries.iteritems():
                if entryID == vehicle._entryID:
                    eve.componentPy.as_updateTextFieldS(vehicleID, "InAoiTf", SPOTSTAT[isInAoI])
                    break

    def onVehicleAdd(self, vehicleID, vehicleType):
        enemy = self.isEnemy(vehicleID)
        eve.componentPy.as_AddTextFieldS(vehicleID, "InAoiTf", cfg.players_spotted["settings"]["x"], cfg.players_spotted["settings"]["y"], cfg.players_spotted["settings"]["align"], TEAM[enemy])
        if self.hpBarsEnable:
            color = int(self.vClassColors[vehicleType.classTag].replace("#", ""), 16) if cfg.players_bars['hpbarsclassColor'] else COLORS[enemy]
            eve.componentPy.as_AddPPanelBarS(vehicleID, color, self.bar_x, self.bars_y, self.bars_width, self.bars_height, TEAM[enemy])
            cur = vehiclesDic[vehicleID][VEHICLE.CUR]
            max = vehiclesDic[vehicleID][VEHICLE.MAX]
            text = macrosReplace(self.hp_text, {'{{health}}': '%d' % cur, '{{maxHealth}}': '%d' % max, "{{percent}}": "%d" % math.ceil(cur * 100.0 / max)})
            eve.componentPy.as_updatePPanelBarS(vehicleID, cur, max, text)
            eve.componentPy.as_setHPbarsVisibleS(vehicleID, not self.hp_on_alt)
        if cfg.vehicle_types['vehicleClassColorsOnPanels']:
            BigWorld.callback(1.0, partial(eve.componentPy.as_setVehicleClassColorS, vehicleID, self.vClassColors[vehicleType.classTag]))

    def updateLinks(self):
        for vehicleID, vehicle in vehiclesDic.iteritems():
            enemy = self.isEnemy(vehicleID)
            eve.componentPy.as_AddTextFieldS(vehicleID, "InAoiTf", cfg.players_spotted["settings"]["x"], cfg.players_spotted["settings"]["y"], cfg.players_spotted["settings"]["align"], TEAM[enemy])
            if self.hpBarsEnable or cfg.vehicle_types['vehicleClassColorsOnPanels']:
                classTag = sessionProvider.getArenaDP().getVehicleInfo(vehicleID).vehicleType.classTag
                if cfg.vehicle_types['vehicleClassColorsOnPanels']:
                    eve.componentPy.as_setVehicleClassColorS(vehicleID, self.vClassColors[classTag])
                if self.hpBarsEnable:
                    curr = vehicle[VEHICLE.CUR]
                    max = vehicle[VEHICLE.MAX]
                    color = int(self.vClassColors[classTag].replace("#", ""), 16) if cfg.players_bars['hpbarsclassColor'] else COLORS[enemy]
                    eve.componentPy.as_AddPPanelBarS(vehicleID, color, self.bar_x, self.bars_y, self.bars_width, self.bars_height, TEAM[enemy])
                    text = macrosReplace(self.hp_text, {'{{health}}': '%d' % curr, '{{maxHealth}}': '%d' % max, "{{percent}}": "%d" % math.ceil(curr * 100.0 / max)})
                    eve.componentPy.as_updatePPanelBarS(vehicleID, curr, max, text)
                    eve.componentPy.as_setHPbarsVisibleS(vehicleID, not self.hp_on_alt)

    def healthOnAlt(self, enable):
        if self.hpBarsEnable:
            for vehicleID, vehicle in vehiclesDic.iteritems():
                if vehicle[VEHICLE.CUR]:
                    eve.componentPy.as_setHPbarsVisibleS(vehicleID, enable)

    def drawGraphics(self, vehicleID, vehicle):
        curr = vehicle[VEHICLE.CUR]
        if self.hpBarsEnable and curr:
            max = vehicle[VEHICLE.MAX]
            text = macrosReplace(self.hp_text, {'{{health}}': '%d' % curr, '{{maxHealth}}': '%d' % max, "{{percent}}": "%d" % math.ceil(curr * 100.0 / max)})
            eve.componentPy.as_updatePPanelBarS(vehicleID, curr, max, text)