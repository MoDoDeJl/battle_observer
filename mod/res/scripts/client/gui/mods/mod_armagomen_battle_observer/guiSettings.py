# -*- coding: utf-8 -*-
import BigWorld
import traceback
from functools import partial
from Core import DEFAULT_LANGUAGE
from Config import cfg, c_Loader
from i18nBO import geti18nDic

from gui.modsListApi import g_modsListApi
from gui.vxSettingsApi import vxSettingsApi, vxSettingsApiEvents

settingsVersion = 20
A_LIST = ["left", "center", "right"]

class ConfigInterface(object):
    __slots__ = ('i18n', 'modID', 'blockIDs', 'stileSelect', 'selectedConfig', 'oldConfig', 'configSelect')
    def __init__(self):
        self.i18n = geti18nDic(DEFAULT_LANGUAGE)
        self.modID = 'BOconfigS'
        self.blockIDs = ('configSelect', 'main', 'debugPanel', 'battleTimer', 'hp_bars', 'markers', 'armor_calculator', 'log_global',
            'log_total', 'log_damage_extended', 'log_input_extended', 'main_gun', 'team_bases_panel',
            'vehicle_types', 'players_spotted', 'players_damages', 'players_bars', 'zoom',
            'arcadeCamera', 'strategicCamera', 'colors', 'flight_time', 'save_shoot', 'minimap')
        self.stileSelect = ('normal', 'legue')
        self.selectedConfig = c_Loader.configsList.index(c_Loader.cName)
        self.oldConfig = self.selectedConfig
        self.configSelect = False
        self.startLoad()

    def modsListRegister(self):
        kwargs = {
         'id':self.modID, 'name':self.i18n['gui_name'],
         'description':self.i18n['gui_description'],
         'icon':'scripts/client/gui/mods/mod_armagomen_battle_observer/A679033ADCCBD3643A61A1CCD.png',
         'enabled':True, 'login':True, 'lobby':True, 'callback':self.MSAPopulate}
        try:
            BigWorld.g_modsListApi.addModification(**kwargs)
        except AttributeError:
            BigWorld.g_modsListApi.addMod(**kwargs)

    def startLoad(self):
        try:
            if not hasattr(BigWorld, 'g_modsListApi'):
                BigWorld.g_modsListApi = g_modsListApi
            CONTAINER = {key: self.i18n['gui_%s' % key] for key in ('windowTitle', 'buttonOK', 'buttonCancel', 'buttonApply', 'enableButtonTooltip')}
            vxSettingsApi.addContainer(self.modID, CONTAINER)
            vxSettingsApi.onFeedbackReceived += self.onFeedbackReceived
            vxSettingsApi.onSettingsChanged += self.onSettingsChanged
            self.modsListRegister()
            vxSettingsApi.onDataChanged += self.dataChangeHandler
            for blockID in self.blockIDs:
                vxSettingsApi.addMod(self.modID, blockID, partial(self.template_settings, blockID), self.getDataBlock(blockID), partial(self.apply_settings, blockID))
        except StandardError:
            traceback.print_exc()

    def MSAPopulate(self):
        self.update_settings()
        vxSettingsApi.loadWindow(self.modID)

    def onFeedbackReceived(self, container, event):
        if container != self.modID:
            return
        if event == vxSettingsApiEvents.WINDOW_CLOSED:
            if self.configSelect:
                c_Loader.createFileInDir(c_Loader.path + 'load.json', {'loadConfig': c_Loader.configsList[self.selectedConfig]})
                c_Loader.readConfig(c_Loader.configsList[self.selectedConfig])
                self.MSAPopulate()
                self.configSelect = False
                self.oldConfig = self.selectedConfig

    @staticmethod
    def _toggleObject(obj, active):
        obj.alpha = 0.40 if not active else 1.0
        obj.mouseEnabled = active
        obj.mouseChildren = active
        obj.tabEnabled = active

    def getDataBlock(self, blockID):
        if blockID == 'configSelect':
            return {'selectedConfig': self.selectedConfig}
        else:
            return getattr(cfg, blockID)

    def updateMod(self, blockID):
        vxSettingsApi.updateMod(self.modID, blockID, partial(self.template_settings, blockID))

    def getLabel(self, varName, ctx='setting'):
        return self.i18n['UI_%s_%s_text' % (ctx, varName)]

    def createTooltip(self, varName, ctx='setting'):
        if self.i18n.get('UI_%s_%s_tooltip' % (ctx, varName), False):
            return '{HEADER}%s{/HEADER}{BODY}%s{/BODY}' % tuple(self.i18n['UI_%s_%s_%s' % (ctx, varName, strType)] for strType in ('text', 'tooltip'))
        else:
            return ''

    def createLabel(self, blockID, varName, ctx='setting'):
        return {'type': 'Label', 'text': self.getLabel('_'.join((blockID, varName)), ctx),
                'tooltip': self.createTooltip('_'.join((blockID, varName)), ctx),
                'label': self.getLabel('_'.join((blockID, varName)), ctx)}

    @staticmethod
    def createEmpty():
        return {'type': 'Empty'}

    def createControl(self, blockID, varName, value, contType='CheckBox', width=200, empty=False, button=None, defaultSelection=False):
        result = self.createLabel(blockID, varName) if not empty else {}
        result.update({'type': contType, 'value': value, 'varName': varName, 'width': width, 'showInBattle': False})
        if contType == 'TextInputField':
            result.update({'defaultSelection': defaultSelection})
        if button is not None:
            result['button'] = button
        return result

    def createDropdown(self, blockID, varName, options, value, contType='Dropdown', width=200, empty=False, button=None):
        result = self.createControl(blockID, varName, value, contType, width, empty, button)
        result.update({'width': width, 'itemRenderer': 'DropDownListItemRendererSound', 'options': [{'label': x} for x in options]})
        return result

    def createHotKey(self, blockID, varName, value, defaultValue, empty=False):
        result = self.createControl(blockID, varName, value, 'HotKey', empty=empty)
        result['defaultValue'] = defaultValue
        return result

    def _createNumeric(self, blockID, varName, contType, value, vMin=0, vMax=0, width=200, empty=False, button=None):
        result = self.createControl(blockID, varName, value, contType, width, empty, button)
        result.update({'minimum': vMin, 'maximum': vMax})
        return result

    def createStepper(self, blockID, varName, vMin, vMax, step, value, manual=True, width=200, empty=False, button=None):
        result = self._createNumeric(blockID, varName, 'NumericStepper', value, vMin, vMax, width, empty, button)
        result.update({'stepSize': step, 'canManualInput': manual})
        return result

    def createSlider(self, blockID, varName, vMin, vMax, step, value, formatStr='{{value}}', width=200, empty=False, button=None):
        result = self._createNumeric(blockID, varName, 'Slider', value, vMin, vMax, width, empty, button)
        result.update({'snapInterval': step, 'format': formatStr})
        return result

    def createRangeSlider(self, blockID, varName, vMin, vMax, labelStep, divStep, step, minRange, value, width=200, empty=False, button=None):
        result = self._createNumeric(blockID, varName, 'RangeSlider', value, vMin, vMax, width, empty, button)
        result.update({'snapInterval': step, 'divisionLabelStep': labelStep, 'divisionStep': divStep, 'minRangeDistance': minRange})
        return result

    def update_settings(self):
        for blockID in self.blockIDs:
            self.updateMod(blockID)

    def onSettingsChanged(self, container, blockID, settings):
        if self.oldConfig != self.selectedConfig or self.modID != container:
            return
        for setting in settings:
            if isinstance(settings[setting], unicode):
                settings[setting] = settings[setting].replace("\\t", "<tab>").replace("\\n", "<br>").replace("\\r", "<br>")
        if blockID == 'configSelect':
            if settings['selectedConfig'] != self.selectedConfig:
                self.oldConfig = self.selectedConfig
                self.selectedConfig = settings['selectedConfig']
                self.configSelect = True
                vxSettingsApi.processEvent(container, vxSettingsApiEvents.CALLBACKS.CLOSE_WINDOW)
        elif blockID == 'main':
            for setting in settings:
                if setting in cfg.main:
                    cfg.main[setting] = settings[setting]
        elif blockID == 'debugPanel':
            for setting in settings:
                cfg.debugPanel[setting] = settings[setting]
        elif blockID == 'battleTimer':
            for setting in settings:
                cfg.battleTimer[setting] = settings[setting]
        elif blockID == 'hp_bars':
            for setting in settings:
                if setting in cfg.hp_bars:
                    if setting == 'style' and not isinstance(settings[setting], unicode):
                        cfg.hp_bars[setting] = self.stileSelect[settings[setting]]
                    else:
                        cfg.hp_bars[setting] = settings[setting]
        elif blockID == 'markers':
            for setting in settings:
                if 'position' in setting:
                    sett = setting.split('*')
                    cfg.markers[sett[0]][sett[1]][sett[2]] = settings[setting]
                elif setting == 'showMarkers_KEY':
                    set = [] if not settings[setting] else settings[setting][0] if isinstance(settings[setting][0], list) else [settings[setting][0]]
                    cfg.markers[setting] = set
                elif setting in cfg.markers:
                    cfg.markers[setting] = settings[setting]
        elif blockID == 'armor_calculator':
            for setting in settings:
                if 'Position' in setting or 'CalculatorText' in setting:
                    sett = setting.split('*')
                    cfg.armor_calculator[sett[0]][sett[1]] = settings[setting]
                else:
                    cfg.armor_calculator[setting] = settings[setting]
        elif blockID == 'log_global':
            for setting in settings:
                if 'attackReason' in setting:
                    sett = setting.split('*')
                    cfg.log_global[sett[0]][sett[1]] = settings[setting]
                elif setting == 'logsAltmodeKey':
                    set = [] if not settings[setting] else settings[setting][0] if isinstance(settings[setting][0], list) else [settings[setting][0]]
                    cfg.log_global[setting] = set
                elif setting != 'enabled':
                    cfg.log_global[setting] = settings[setting]
        elif blockID == 'log_total':
            for setting in settings:
                cfg.log_total[setting] = settings[setting]
        elif blockID == 'log_damage_extended':
            for setting in settings:
                if 'damageLogPosition' in setting or 'shellTypes' in setting or 'shellColor' in setting:
                    sett = setting.split('*')
                    cfg.log_damage_extended[sett[0]][sett[1]] = settings[setting]
                else:
                    cfg.log_damage_extended[setting] = settings[setting]
        elif blockID == 'log_input_extended':
            for setting in settings:
                if 'inputLogPosition' in setting or 'shellTypes' in setting or 'shellColor' in setting:
                    sett = setting.split('*')
                    cfg.log_input_extended[sett[0]][sett[1]] = settings[setting]
                else:
                    cfg.log_input_extended[setting] = settings[setting]
        elif blockID == 'main_gun':
            for setting in settings:
                cfg.main_gun[setting] = settings[setting]
        elif blockID == 'team_bases_panel':
            for setting in settings:
                cfg.team_bases_panel[setting] = settings[setting]
        elif blockID == 'vehicle_types':
            for setting in settings:
                if setting == 'vehicleClassColorsOnPanels':
                    cfg.vehicle_types[setting] = settings[setting]
                elif setting != 'enabled':
                    sett = setting.split('*')
                    cfg.vehicle_types[sett[0]][sett[1]] = settings[setting]
        elif blockID == 'players_spotted':
            for setting in settings:
                if setting == 'enabled':
                    cfg.players_spotted[setting] = settings[setting]
                else:
                    sett = setting.split('*')
                    if 'align' in setting:
                        cfg.players_spotted[sett[0]][sett[1]] = A_LIST[settings[setting]]
                    else:
                        cfg.players_spotted[sett[0]][sett[1]] = settings[setting]
        elif blockID == 'players_damages':
            for setting in settings:
                if 'damages_settings' in setting:
                    sett = setting.split('*')
                    if 'align' in setting:
                        cfg.players_damages[sett[0]][sett[1]] = A_LIST[settings[setting]]
                    else:
                        cfg.players_damages[sett[0]][sett[1]] = settings[setting]
                elif setting == 'damages_key':
                    set = [] if not settings[setting] else settings[setting][0] if isinstance(settings[setting][0], list) else [settings[setting][0]]
                    cfg.players_damages[setting] = set
                else:
                    cfg.players_damages[setting] = settings[setting]
        elif blockID == 'players_bars':
            for setting in settings:
                if '_settings'in setting:
                    sett = setting.split('*')
                    cfg.players_bars[sett[0]][sett[1]] = settings[setting]
                elif setting == 'hpbarsShowKey':
                    set = [] if not settings[setting] else settings[setting][0] if isinstance(settings[setting][0], list) else [settings[setting][0]]
                    cfg.players_bars['hpbarsShowKey'] = set
                else:
                    cfg.players_bars[setting] = settings[setting]
        elif blockID == 'zoom':
            for setting in settings:
                if setting == 'enabled':
                    cfg.zoom[setting] = settings[setting]
                elif setting == 'zoomSteps':
                    cfg.zoom[setting] = [round(float(x), 1) for x in settings[setting].replace(' ', '').split(',')]
                elif setting == 'LENS_EFFECTS_ENABLED':
                    cfg.zoom[setting] = not settings[setting]
                else:
                    sett = setting.split('*')
                    cfg.zoom[sett[0]][sett[1]] = float(settings[setting]) if type(settings[setting]) == int else settings[setting]
        elif blockID == 'arcadeCamera':
            for setting in settings:
                cfg.arcadeCamera[setting] = settings[setting]
        elif blockID == 'strategicCamera':
            for setting in settings:
                cfg.strategicCamera[setting] = settings[setting]
        elif blockID == 'colors':
            for setting in settings:
                if setting != 'enabled':
                    sett = setting.split('*')
                    cfg.colors[sett[0]][sett[1]] = settings[setting]
        elif blockID == 'flight_time':
            for setting in settings:
                cfg.flight_time[setting] = settings[setting]
        elif blockID == 'save_shoot':
            for setting in settings:
                cfg.save_shoot[setting] = settings[setting]
        elif blockID == 'minimap':
            for setting in settings:
                if setting != 'enabled':
                    cfg.minimap[setting] = [] if not settings[setting] else settings[setting][0] if isinstance(settings[setting][0], list) else [settings[setting][0]]
                else:
                    cfg.minimap[setting] = settings[setting]                
        if blockID != 'configSelect':
            c_Loader.createFileInDir("%s%s/%s.json" % (c_Loader.path, c_Loader.configsList[self.selectedConfig], blockID), getattr(cfg, blockID))

    def apply_settings(self, blockID, settings):
        if self.oldConfig != self.selectedConfig:
            return
        self.updateMod(blockID)

    def dataChangeHandler(self, modID, ID, varName, value, *a, **k):
        if modID != self.modID or ID not in self.blockIDs:
            return
        getObject = vxSettingsApi.getDAAPIObject
        if ID == 'zoom' and varName == 'dynamyc_zoom*enable':
            for var in ('dynamyc_zoom*maxZoom', 'dynamyc_zoom*minZoom', 'dynamyc_zoom*zoomXMetrs'):
                self._toggleObject(getObject(ID, var), value)
            self._toggleObject(getObject(ID, 'default_zoom*enable'), not value)

    def template_settings(self, blockID):
        if blockID == 'configSelect':
            return {
                'modDisplayName': self.i18n['UI_setting_header_selectConfig'],
                'settingsVersion': settingsVersion,
                'enabled': True,
                'showToggleButton': False,
                'inBattle': False,
                'position': 0,
                'column1': [self.createDropdown(blockID, 'selectedConfig', c_Loader.configsList, self.selectedConfig, empty=True)],
                'column2': []
                }
        elif blockID == 'hp_bars':
            return {
                'modDisplayName': self.i18n['UI_setting_header_hp_panels_1'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.hp_bars["enabled"],
                'showToggleButton': True,
                'inBattle': False,
                'position': 1,
                'column1': [self.createSlider(blockID, 'barsWidth', 100, 400, 2, cfg.hp_bars['barsWidth']),
                    self.createControl(blockID, 'showAliveCount', cfg.hp_bars['showAliveCount'])],
                'column2': [self.createDropdown(blockID, 'style', self.stileSelect, self.stileSelect.index(cfg.hp_bars['style'])),
                    self.createControl(blockID, 'differenceHP', cfg.hp_bars['differenceHP'])]
                }
        elif blockID == 'main':
            return {
                'modDisplayName': self.i18n['UI_setting_header_main'],
                'settingsVersion': settingsVersion,
                'enabled': True,
                'showToggleButton': False,
                'inBattle': False,
                'position': 2,
                'column1': [self.createControl(blockID, 'PYTHON_LOG_ANTISPAM', cfg.main['PYTHON_LOG_ANTISPAM']),
                    self.createControl(blockID, 'CHECK_UPDATES', cfg.main['CHECK_UPDATES']),
                    self.createControl(blockID, 'hideChatInRandom', cfg.main['hideChatInRandom']),
                    self.createControl(blockID, 'hideBadges', cfg.main['hideBadges'])],
                'column2': [self.createControl(blockID, 'background', cfg.main['background']),
                    self.createSlider(blockID, 'backgroundTransparency', 0, 1, 0.05, cfg.main['backgroundTransparency'])]
                }
        elif blockID == 'debugPanel':
            return {
                'modDisplayName': self.i18n['UI_setting_header_Debug'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.debugPanel['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 3,
                'column1': [
                    self.createSlider(blockID, 'position_x', 0, 2000, 1, cfg.debugPanel['position_x']),
                    self.createControl(blockID, 'debugTemplate', cfg.debugPanel['debugTemplate'], 'TextInputField', 800),
                    self.createSlider(blockID, 'scale', 0.1, 1.0, 0.05, cfg.debugPanel['scale'])
                    ],
                'column2': [
                    self.createSlider(blockID, 'position_y', 0, 2000, 1, cfg.debugPanel['position_y'])
                    ]
                }
        elif blockID == 'battleTimer':
            return {
                'modDisplayName': self.i18n['UI_setting_header_Timer'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.battleTimer['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 4,
                'column1': [
                    self.createControl(blockID, 'clockFormat', cfg.battleTimer['clockFormat'], 'TextInputField',350),
                    self.createControl(blockID, 'clockTemplate', cfg.battleTimer['clockTemplate'], 'TextInputField', 350),
                    self.createControl(blockID, 'timerTemplate', cfg.battleTimer['timerTemplate'], 'TextInputField', 350)
                    ],
                'column2': []
                }
        elif blockID == 'markers':
            key = cfg.markers['showMarkers_KEY']
            if type(key) != list or not key:
                print key
                key = [82]
                c_Loader.createFileInDir("%s%s/markers.json" % (c_Loader.path, c_Loader.configsList[self.selectedConfig]), cfg.markers)

            return {
                'modDisplayName': self.i18n['UI_setting_header_markers'],
                'settingsVersion': settingsVersion,
                'enabled': True,
                'showToggleButton': False,
                'inBattle': False,
                'position': 5,
                'column1': [
                    self.createSlider(blockID, 'position*ally_x*normal', -200, 0, 1, cfg.markers['position']['ally_x']['normal']),
                    self.createSlider(blockID, 'position*ally_y*normal', 0, 100, 1, cfg.markers['position']['ally_y']['normal']),
                    self.createSlider(blockID, 'position*ally_x*legue', -200, 0, 1, cfg.markers['position']['ally_x']['legue']),
                    self.createSlider(blockID, 'position*ally_y*legue', 0, 100, 1, cfg.markers['position']['ally_y']['legue']),
                    self.createHotKey(blockID, 'showMarkers_KEY', key if len(key) == 1 else [key], [82])
                    ],
                'column2': [
                    self.createSlider(blockID, 'position*enemy_x*normal', 0, 200, 1, cfg.markers['position']['enemy_x']['normal']),
                    self.createSlider(blockID, 'position*enemy_y*normal', 0, 100, 1, cfg.markers['position']['enemy_y']['normal']),
                    self.createSlider(blockID, 'position*enemy_x*legue', 0, 200, 1, cfg.markers['position']['enemy_x']['legue']),
                    self.createSlider(blockID, 'position*enemy_y*legue', 0, 100, 1, cfg.markers['position']['enemy_y']['legue']),
                    self.createControl(blockID, 'markersClassColor', cfg.markers['markersClassColor'])
                    ]
                }
        elif blockID == 'armor_calculator':
            return {
                'modDisplayName': self.i18n['UI_setting_header_calculator'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.armor_calculator['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 6,
                'column1': [
                    self.createSlider(blockID, 'calcPosition*x', -300, 300, 1, cfg.armor_calculator['calcPosition']['x']),
                    self.createSlider(blockID, 'calcTextPosition*x', -300, 300, 1, cfg.armor_calculator['calcTextPosition']['x']),
                    # self.createControl(blockID, 'extended', cfg.armor_calculator['extended']),
                    self.createControl(blockID, 'showCalcPoints', cfg.armor_calculator['showCalcPoints']),
                    self.createControl(blockID, 'showText', cfg.armor_calculator['showText']),
                    self.createControl(blockID, 'template', cfg.armor_calculator['template'], 'TextInputField', 800),
                    self.createControl(blockID, 'armorCalculatorText*green', cfg.armor_calculator['armorCalculatorText']['green'], 'TextInputField', 400),
                    self.createControl(blockID, 'armorCalculatorText*orange', cfg.armor_calculator['armorCalculatorText']['orange'], 'TextInputField', 400),
                    self.createControl(blockID, 'armorCalculatorText*red', cfg.armor_calculator['armorCalculatorText']['red'], 'TextInputField', 400),
                    self.createControl(blockID, 'armorCalculatorText*yellow', cfg.armor_calculator['armorCalculatorText']['yellow'], 'TextInputField', 400),
                    self.createControl(blockID, 'armorCalculatorText*purple', cfg.armor_calculator['armorCalculatorText']['purple'], 'TextInputField', 400)
                    ],
                'column2': [
                    self.createSlider(blockID, 'calcPosition*y', -300, 300, 1, cfg.armor_calculator['calcPosition']['y']),
                    self.createSlider(blockID, 'calcTextPosition*y', -300, 300, 1, cfg.armor_calculator['calcTextPosition']['y']),
                    ]
                }
        elif blockID == 'log_global':
            key = cfg.log_global['logsAltmodeKey']
            if type(key) != list or not key:
                print key
                key = [56, 184]
                c_Loader.createFileInDir("%s%s/damage_log.json" % (c_Loader.path, c_Loader.configsList[self.selectedConfig]), cfg.damage_log)

            return {
                'modDisplayName': self.i18n['UI_setting_header_log*all'],
                'settingsVersion': settingsVersion,
                'enabled': True,
                'showToggleButton': False,
                'inBattle': False,
                'position': 7,
                'column1': [
                    self.createControl(blockID, 'wg_log_pos_fix', cfg.log_global['wg_log_pos_fix']),
                    self.createControl(blockID, 'wg_log_hide_crits', cfg.log_global['wg_log_hide_crits']),
                    self.createLabel(blockID, 'attackReas'),
                    self.createControl(blockID, 'attackReason*shot', cfg.log_global['attackReason']['shot'], 'TextInputField', 800),
                    self.createControl(blockID, 'attackReason*fire', cfg.log_global['attackReason']['fire'], 'TextInputField', 800),
                    self.createControl(blockID, 'attackReason*ramming', cfg.log_global['attackReason']['ramming'], 'TextInputField', 800),
                    self.createControl(blockID, 'attackReason*world_collision', cfg.log_global['attackReason']['world_collision'], 'TextInputField', 800),
                    self.createControl(blockID, 'attackReason*drowning', cfg.log_global['attackReason']['drowning'], 'TextInputField', 800),
                    self.createControl(blockID, 'attackReason*overturn', cfg.log_global['attackReason']['overturn'], 'TextInputField', 800),
                    self.createEmpty(),
                    self.createHotKey(blockID, 'logsAltmodeKey', key if len(key) == 1 else [key], [[56, 184]])
                    ],
                'column2': [
                    self.createControl(blockID, 'wg_log_hide_block', cfg.log_global['wg_log_hide_block']),
                    self.createControl(blockID, 'wg_log_hide_assist', cfg.log_global['wg_log_hide_assist'])
                    ]
                }
        elif blockID == 'log_total':
            return {
                'modDisplayName': self.i18n['UI_setting_header_log*total'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.log_total['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 8,
                'column1': [
                    self.createSlider(blockID, 'logPosition_x', 0, 2000, 1, cfg.log_total['logPosition_x']),
                    self.createSlider(blockID, 'logPosition_y', 0, 2000, 1, cfg.log_total['logPosition_y']),
                    self.createControl(blockID, 'damageIcon', cfg.log_total['damageIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'blockedIcon', cfg.log_total['blockedIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'assistIcon', cfg.log_total['assistIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'spottedIcon', cfg.log_total['spottedIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'stunIcon', cfg.log_total['stunIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'templateMainDMG', cfg.log_total['templateMainDMG'], 'TextInputField', 800)
                    ],
                'column2': [
                    self.createSlider(blockID, 'mainLogScale', 0, 2, 0.1, cfg.log_total['mainLogScale'])
                    ]
                }
        elif blockID == 'log_damage_extended':
            return {
                'modDisplayName': self.i18n['UI_setting_header_log*damage'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.log_damage_extended['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 9,
                'column1': [
                    self.createSlider(blockID, 'damageLogPosition*x', 0, 2000, 1, cfg.log_damage_extended['damageLogPosition']['x']),
                    self.createControl(blockID, 'shellTypes*ARMOR_PIERCING', cfg.log_damage_extended['shellTypes']['ARMOR_PIERCING'], 'TextInputField', 350),
                    self.createControl(blockID, 'shellTypes*ARMOR_PIERCING_CR', cfg.log_damage_extended['shellTypes']['ARMOR_PIERCING_CR'], 'TextInputField', 350),
                    self.createControl(blockID, 'shellTypes*ARMOR_PIERCING_HE', cfg.log_damage_extended['shellTypes']['ARMOR_PIERCING_HE'], 'TextInputField', 350),
                    self.createControl(blockID, 'reverse', cfg.log_damage_extended['reverse']),
                    self.createControl(blockID, 'startTag', cfg.log_damage_extended['startTag'], 'TextInputField', 800),
                    self.createControl(blockID, 'extendedLog', cfg.log_damage_extended['extendedLog'], 'TextInputField', 800),
                    self.createControl(blockID, 'extendedLogALTMODE', cfg.log_damage_extended['extendedLogALTMODE'], 'TextInputField', 800),
                    self.createControl(blockID, 'endTag', cfg.log_damage_extended['endTag'], 'TextInputField', 800),
                    self.createControl(blockID, 'killedIcon', cfg.log_damage_extended['killedIcon'], 'TextInputField', 800),
                    ],
                'column2': [
                    self.createSlider(blockID, 'damageLogPosition*y', 0, 2000, 1, cfg.log_damage_extended['damageLogPosition']['y']),
                    self.createControl(blockID, 'shellTypes*HIGH_EXPLOSIVE', cfg.log_damage_extended['shellTypes']['HIGH_EXPLOSIVE'], 'TextInputField', 350),
                    self.createControl(blockID, 'shellTypes*HOLLOW_CHARGE', cfg.log_damage_extended['shellTypes']['HOLLOW_CHARGE'], 'TextInputField', 350),
                    self.createControl(blockID, 'shellTypes*UNDEFINED', cfg.log_damage_extended['shellTypes']['UNDEFINED'], 'TextInputField', 350)
                    ]
                }
        elif blockID == 'log_input_extended':
            return {
                'modDisplayName': self.i18n['UI_setting_header_log*inputDlog'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.log_input_extended['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 10,
                'column1': [
                    self.createSlider(blockID, 'inputLogPosition*x', 0, 2000, 1, cfg.log_input_extended['inputLogPosition']['x']),
                    self.createControl(blockID, 'shellTypes*ARMOR_PIERCING', cfg.log_input_extended['shellTypes']['ARMOR_PIERCING'], 'TextInputField', 350),
                    self.createControl(blockID, 'shellTypes*ARMOR_PIERCING_CR', cfg.log_input_extended['shellTypes']['ARMOR_PIERCING_CR'], 'TextInputField', 350),
                    self.createControl(blockID, 'shellTypes*ARMOR_PIERCING_HE', cfg.log_input_extended['shellTypes']['ARMOR_PIERCING_HE'], 'TextInputField', 350),
                    self.createControl(blockID, 'reverse', cfg.log_input_extended['reverse']),
                    self.createControl(blockID, 'startTag', cfg.log_input_extended['startTag'], 'TextInputField', 800),
                    self.createControl(blockID, 'extendedLog', cfg.log_input_extended['extendedLog'], 'TextInputField', 800),
                    self.createControl(blockID, 'extendedLogALTMODE', cfg.log_input_extended['extendedLogALTMODE'], 'TextInputField', 800),
                    self.createControl(blockID, 'endTag', cfg.log_input_extended['endTag'], 'TextInputField', 800),
                    self.createControl(blockID, 'killedIcon', cfg.log_input_extended['killedIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'shellColor*normal', cfg.log_input_extended['shellColor']['normal'], 'TextInputColor', 100),
                    self.createControl(blockID, 'shellColor*gold', cfg.log_input_extended['shellColor']['gold'], 'TextInputColor', 100)
                    ],
                'column2': [
                    self.createSlider(blockID, 'inputLogPosition*y', 0, 2000, 1, cfg.log_input_extended['inputLogPosition']['y']),
                    self.createControl(blockID, 'shellTypes*HIGH_EXPLOSIVE', cfg.log_input_extended['shellTypes']['HIGH_EXPLOSIVE'], 'TextInputField', 350),
                    self.createControl(blockID, 'shellTypes*HOLLOW_CHARGE', cfg.log_input_extended['shellTypes']['HOLLOW_CHARGE'], 'TextInputField', 350),
                    self.createControl(blockID, 'shellTypes*UNDEFINED', cfg.log_input_extended['shellTypes']['UNDEFINED'], 'TextInputField', 350)
                    ]
                }
        elif blockID == 'main_gun':
            return {
                'modDisplayName': self.i18n['UI_setting_header_main_gun'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.main_gun['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 11,
                'column1': [
                    self.createSlider(blockID, 'position_x', -1000, 1000, 1, cfg.main_gun['position_x']),
                    self.createControl(blockID, 'mainGunDynamic', cfg.main_gun['mainGunDynamic']),
                    self.createControl(blockID, 'mainGunIcon', cfg.main_gun['mainGunIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'mainGunDoneIcon', cfg.main_gun['mainGunDoneIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'mainGunFailureIcon', cfg.main_gun['mainGunFailureIcon'], 'TextInputField', 800),
                    self.createControl(blockID, 'template', cfg.main_gun['template'], 'TextInputField', 800)

                    ],
                'column2': [
                    self.createSlider(blockID, 'position_y', 0, 2000, 1, cfg.main_gun['position_y'])
                    ]
                }
        elif blockID == 'team_bases_panel':
            return {
                'modDisplayName': self.i18n['UI_setting_header_team_bases'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.team_bases_panel['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 12,
                'column1': [self.createControl(blockID, 'boBases', cfg.team_bases_panel['boBases']),
                    self.createSlider(blockID, 'y', 0, 1000, 1, cfg.team_bases_panel['y'])],
                'column2': [self.createSlider(blockID, 'scale', 0.5, 1.5, 0.05, cfg.team_bases_panel['scale'])]
                }
        elif blockID == 'vehicle_types':
            return {
                'modDisplayName': self.i18n['UI_setting_header_vehicle_types'],
                'settingsVersion': settingsVersion,
                'enabled': True,
                'showToggleButton': False,
                'inBattle': False,
                'position': 13,
                'column1': [self.createControl(blockID, 'vehicleClassIcon*heavyTank', cfg.vehicle_types['vehicleClassIcon']['heavyTank'], 'TextInputField', 400),
                    self.createControl(blockID, 'vehicleClassIcon*mediumTank', cfg.vehicle_types['vehicleClassIcon']['mediumTank'], 'TextInputField', 400),
                    self.createControl(blockID, 'vehicleClassIcon*AT-SPG', cfg.vehicle_types['vehicleClassIcon']['AT-SPG'], 'TextInputField', 400),
                    self.createControl(blockID, 'vehicleClassIcon*SPG', cfg.vehicle_types['vehicleClassIcon']['SPG'], 'TextInputField', 400),
                    self.createControl(blockID, 'vehicleClassIcon*lightTank', cfg.vehicle_types['vehicleClassIcon']['lightTank'], 'TextInputField', 400),
                    self.createControl(blockID, 'vehicleClassIcon*unknown', cfg.vehicle_types['vehicleClassIcon']['unknown'], 'TextInputField', 400),
                    self.createEmpty(),
                    self.createControl(blockID, 'vehicleClassColorsOnPanels', cfg.vehicle_types['vehicleClassColorsOnPanels'])
                    ],
                'column2': [self.createEmpty(), self.createEmpty(),
                    self.createControl(blockID, 'vehicleClassColors*heavyTank', cfg.vehicle_types['vehicleClassColors']['heavyTank'], 'TextInputColor', 100),
                    self.createEmpty(), self.createEmpty(),
                    self.createControl(blockID, 'vehicleClassColors*mediumTank', cfg.vehicle_types['vehicleClassColors']['mediumTank'], 'TextInputColor', 100),
                    self.createEmpty(), self.createEmpty(),
                    self.createControl(blockID, 'vehicleClassColors*AT-SPG', cfg.vehicle_types['vehicleClassColors']['AT-SPG'], 'TextInputColor', 100),
                    self.createEmpty(), self.createEmpty(),
                    self.createControl(blockID, 'vehicleClassColors*SPG', cfg.vehicle_types['vehicleClassColors']['SPG'], 'TextInputColor', 100),
                    self.createEmpty(), self.createEmpty(),
                    self.createControl(blockID, 'vehicleClassColors*lightTank', cfg.vehicle_types['vehicleClassColors']['lightTank'], 'TextInputColor', 100),
                    self.createEmpty(), self.createEmpty(),
                    self.createControl(blockID, 'vehicleClassColors*unknown', cfg.vehicle_types['vehicleClassColors']['unknown'], 'TextInputColor', 100)
                    ]
                }
        elif blockID == 'players_spotted':
            align = cfg.players_spotted['settings']['align']
            return {
                'modDisplayName': self.i18n['UI_setting_header_spotted'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.players_spotted['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 14,
                'column1': [self.createSlider(blockID, 'settings*x', -300, 300, 1, cfg.players_spotted['settings']['x']),
                    self.createDropdown(blockID, 'settings*align', A_LIST, A_LIST.index(align if align in A_LIST else 0)),
                    self.createControl(blockID, 'status*lights', cfg.players_spotted['status']['lights'], 'TextInputField', 800),
                    self.createControl(blockID, 'status*donotlight', cfg.players_spotted['status']['donotlight'], 'TextInputField', 800)
                    ],
                'column2': [self.createSlider(blockID, 'settings*y', -20, 20, 1, cfg.players_spotted['settings']['y'])]
                }
        elif blockID == 'players_damages':
            key = cfg.players_damages['damages_key']
            if type(key) != list or not key:
                print key
                key = [48]
                c_Loader.createFileInDir("%s%s/players_damages.json" % (c_Loader.path, c_Loader.configsList[self.selectedConfig]), cfg.players_damages)
            align = cfg.players_damages['damages_settings']['align']
            return {
                'modDisplayName': self.i18n['UI_setting_header_damages'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.players_damages['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 15,
                'column1': [self.createSlider(blockID, 'damages_settings*x', -300, 300, 1, cfg.players_damages['damages_settings']['x']),
                    self.createDropdown(blockID, 'damages_settings*align', A_LIST, A_LIST.index(align if align in A_LIST else 0)),
                    self.createControl(blockID, 'damages_text', cfg.players_damages['damages_text'], 'TextInputField', 800),
                    self.createHotKey(blockID, 'damages_key', key if len(key) == 1 else [key], [48])],
                'column2': [self.createSlider(blockID, 'damages_settings*y', -20, 20, 1, cfg.players_damages['damages_settings']['y'])]
                }
        elif blockID == 'players_bars':
            key = cfg.players_bars['hpbarsShowKey']
            if type(key) != list or not key:
                print key
                key = [56, 184]
                c_Loader.createFileInDir("%s%s/players_bars.json" % (c_Loader.path, c_Loader.configsList[self.selectedConfig]), cfg.players_bars)

            return {
                'modDisplayName': self.i18n['UI_setting_header_hp_panels_2'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.players_bars['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 16,
                'column1': [
                    self.createControl(blockID, 'hpbarsclassColor', cfg.players_bars['hpbarsclassColor']),
                    self.createEmpty(),
                    self.createSlider(blockID, 'hp_bar_settings*bars_x', -300, 300, 1, cfg.players_bars['hp_bar_settings']['bars_x']),
                    self.createSlider(blockID, 'hp_bar_settings*bars_width', 20, 400, 1, cfg.players_bars['hp_bar_settings']['bars_width']),
                    self.createEmpty(),
                    self.createHotKey(blockID, 'hpbarsShowKey', key if len(key) == 1 else [key], [[56, 184]])
                    ],
                'column2': [
                    self.createControl(blockID, 'showHpBarsOnKeyDown', cfg.players_bars['showHpBarsOnKeyDown']),
                    self.createEmpty(),
                    self.createSlider(blockID, 'hp_bar_settings*bars_y', -30, 30, 1, cfg.players_bars['hp_bar_settings']['bars_y']),
                    self.createSlider(blockID, 'hp_bar_settings*bars_height', 2, 30, 1, cfg.players_bars['hp_bar_settings']['bars_height']),
                    self.createEmpty(),
                    self.createControl(blockID, 'hp_text', cfg.players_bars['hp_text'], 'TextInputField', 400)
                ]
                }
        elif blockID == 'zoom':
            zoomSteps = cfg.zoom['zoomSteps']
            return {
                'modDisplayName': self.i18n['UI_setting_header_zoomX'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.zoom['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 17,
                'column1': [
                    self.createControl(blockID, 'dynamyc_zoom*enable', cfg.zoom['dynamyc_zoom']['enable']),
                    self.createControl(blockID, 'dynamyc_zoom*zoomToGunMarker', cfg.zoom['dynamyc_zoom']['zoomToGunMarker']),
                    self.createSlider(blockID, 'dynamyc_zoom*maxZoom', max(2.0, min(zoomSteps)), min(40.0, max(zoomSteps)), 1.0, cfg.zoom['dynamyc_zoom']['maxZoom']),
                    self.createSlider(blockID, 'dynamyc_zoom*minZoom', max(2.0, min(zoomSteps)), min(40.0, max(zoomSteps)), 1.0, cfg.zoom['dynamyc_zoom']['minZoom']),
                    self.createSlider(blockID, 'dynamyc_zoom*zoomXMetrs', 10, 30, 1, cfg.zoom['dynamyc_zoom']['zoomXMetrs']),
                    self.createControl(blockID, 'zoomSteps', ', '.join((str(x) for x in zoomSteps)), 'TextInputField', 400),
                    self.createControl(blockID, 'default_zoom*enable', cfg.zoom['default_zoom']['enable']),
                    self.createSlider(blockID, 'default_zoom*defZoom', max(2.0, min(zoomSteps)), min(40.0, max(zoomSteps)), 1.0, cfg.zoom['default_zoom']['defZoom']),
                    self.createControl(blockID, 'LENS_EFFECTS_ENABLED', not cfg.zoom['LENS_EFFECTS_ENABLED'])
                    ],
                'column2': []
                }
        elif blockID == 'arcadeCamera':
            return {
                'modDisplayName': self.i18n['UI_setting_header_ArcadeCamera'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.arcadeCamera['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 18,
                'column1': [
                    self.createSlider(blockID, 'min', 2.0, 10.0, 1.0, cfg.arcadeCamera['min']),
                    self.createSlider(blockID, 'max', 25.0, 400.0, 1.0, cfg.arcadeCamera['max']),
                    self.createSlider(blockID, 'startDeadDist', 15.0, 100.0, 1.0, cfg.arcadeCamera['startDeadDist']),
                    ],
                'column2': []
                }
        elif blockID == 'strategicCamera':
            return {
                'modDisplayName': self.i18n['UI_setting_header_StrategicCamera'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.strategicCamera['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 19,
                'column1': [
                    self.createSlider(blockID, 'min', 20.0, 100.0, 1.0, cfg.strategicCamera['min']),
                    self.createSlider(blockID, 'max', 100.0, 400.0, 1.0, cfg.strategicCamera['max'])
                    ],
                'column2': []
                }
        elif blockID == 'colors':
            return {
                'modDisplayName': self.i18n['UI_setting_header_colors'],
                'settingsVersion': settingsVersion,
                'enabled': True,
                'showToggleButton': False,
                'inBattle': False,
                'position': 20,
                'column1': [
                    self.createControl(blockID, 'mainGun*mainGunColor', cfg.colors['mainGun']['mainGunColor'], 'TextInputColor', 100),
                    self.createControl(blockID, 'battleTimer*timerColor', cfg.colors['battleTimer']['timerColor'], 'TextInputColor', 100),
                    self.createControl(blockID, 'battleTimer*timerColorEndBattle', cfg.colors['battleTimer']['timerColorEndBattle'], 'TextInputColor', 100),
                    self.createControl(blockID, 'debug*pingColor', cfg.colors['debug']['pingColor'], 'TextInputColor', 100),
                    self.createControl(blockID, 'debug*pingLagColor', cfg.colors['debug']['pingLagColor'], 'TextInputColor', 100),
                    self.createEmpty(), self.createLabel(blockID, 'bars_colors'),
                    self.createControl(blockID, 'hpBar*alliesHpBarColor', cfg.colors['hpBar']['alliesHpBarColor'], 'TextInputColor', 100),
                    self.createControl(blockID, 'hpBar*enemyHpBarColor', cfg.colors['hpBar']['enemyHpBarColor'], 'TextInputColor', 100),
                    self.createControl(blockID, 'hpBar*enemyHpBarColorBlind', cfg.colors['hpBar']['enemyHpBarColorBlind'], 'TextInputColor', 100),
                    self.createControl(blockID, 'hpBar*backgroundHpBarColor', cfg.colors['hpBar']['backgroundHpBarColor'], 'TextInputColor', 100),
                    self.createSlider(blockID, 'hpBar*bg_bars_alpha', 0, 1, 0.01, cfg.colors['hpBar']['bg_bars_alpha']),
                    self.createSlider(blockID, 'hpBar*hpBarsTransparency', 0, 1, 0.01, cfg.colors['hpBar']['hpBarsTransparency']),
                    self.createEmpty(), self.createLabel(blockID, 'mark_colors'),
                    self.createControl(blockID, 'markers*ally', cfg.colors['markers']['ally'], 'TextInputColor', 100),
                    self.createControl(blockID, 'markers*enemy', cfg.colors['markers']['enemy'], 'TextInputColor', 100),
                    self.createControl(blockID, 'markers*enemyColorBlind', cfg.colors['markers']['enemyColorBlind'], 'TextInputColor', 100),
                    self.createControl(blockID, 'markers*deadColor', cfg.colors['markers']['deadColor'], 'TextInputColor', 100),
                    self.createEmpty(), self.createLabel(blockID, 'colorAvg_colors'),
                    self.createControl(blockID, 'colorAvgDmg*very_bad', cfg.colors['colorAvgDmg']['very_bad'], 'TextInputColor', 100),
                    self.createControl(blockID, 'colorAvgDmg*bad', cfg.colors['colorAvgDmg']['bad'], 'TextInputColor', 100),
                    self.createControl(blockID, 'colorAvgDmg*normal', cfg.colors['colorAvgDmg']['normal'], 'TextInputColor', 100),
                    self.createControl(blockID, 'colorAvgDmg*good', cfg.colors['colorAvgDmg']['good'], 'TextInputColor', 100),
                    self.createControl(blockID, 'colorAvgDmg*very_good', cfg.colors['colorAvgDmg']['very_good'], 'TextInputColor', 100),
                    self.createControl(blockID, 'colorAvgDmg*unique', cfg.colors['colorAvgDmg']['unique'], 'TextInputColor', 100),
                    self.createEmpty(), self.createLabel(blockID, 'attacker_colors'),
                    self.createControl(blockID, 'attackerColor*isSquadMan', cfg.colors['attackerColor']['isSquadMan'], 'TextInputColor', 100),
                    self.createControl(blockID, 'attackerColor*isTeamDamage', cfg.colors['attackerColor']['isTeamDamage'], 'TextInputColor', 100),
                    self.createControl(blockID, 'attackerColor*isEnemy', cfg.colors['attackerColor']['isEnemy'], 'TextInputColor', 100),
                    self.createControl(blockID, 'attackerColor*unknown', cfg.colors['attackerColor']['unknown'], 'TextInputColor', 100),
                    self.createEmpty(), self.createLabel(blockID, 'calculator_colors'),
                    self.createControl(blockID, 'armorCalculatorColor*green', cfg.colors['armorCalculatorColor']['green'], 'TextInputColor', 100),
                    self.createControl(blockID, 'armorCalculatorColor*orange', cfg.colors['armorCalculatorColor']['orange'], 'TextInputColor', 100),
                    self.createControl(blockID, 'armorCalculatorColor*red', cfg.colors['armorCalculatorColor']['red'], 'TextInputColor', 100),
                    self.createControl(blockID, 'armorCalculatorColor*yellow', cfg.colors['armorCalculatorColor']['yellow'], 'TextInputColor', 100),
                    self.createControl(blockID, 'armorCalculatorColor*purple', cfg.colors['armorCalculatorColor']['purple'], 'TextInputColor', 100)],
                'column2': []
                }
        elif blockID == 'flight_time':
            return {
                'modDisplayName': self.i18n['UI_setting_header_flight_time'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.flight_time['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 21,
                'column1': [
                    self.createSlider(blockID, 'pos_x', -1000, 1000, 1, cfg.flight_time['pos_x']),
                    self.createControl(blockID, 'spgOnly', cfg.flight_time['spgOnly']),
                    self.createControl(blockID, 'template', cfg.flight_time['template'], 'TextInputField', 400)
                    ],
                'column2': [
                    self.createSlider(blockID, 'pos_y', -1000, 1000, 1, cfg.flight_time['pos_y'])
                    ]
                }
        elif blockID == 'save_shoot':
            return {
                'modDisplayName': self.i18n['UI_setting_header_save_shoot'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.save_shoot['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 22,
                'column1': [
                    self.createControl(blockID, 'aliveOnly', cfg.save_shoot['aliveOnly']),
                    self.createControl(blockID, 'msg', cfg.save_shoot['msg'], 'TextInputField', 400)
                    ],
                'column2': []
                }
        elif blockID == 'minimap':
            key = cfg.minimap['key']
            if type(key) != list or not key:
                print key
                key = [29, 157]
                c_Loader.createFileInDir("%s%s/minimap.json" % (c_Loader.path, c_Loader.configsList[self.selectedConfig]), cfg.minimap)
            return {
                'modDisplayName': self.i18n['UI_setting_header_minimap'],
                'settingsVersion': settingsVersion,
                'enabled': cfg.minimap['enabled'],
                'showToggleButton': True,
                'inBattle': False,
                'position': 23,
                'column1': [self.createHotKey(blockID, 'key', key if len(key) == 1 else [key], [[29, 157]])],
                'column2': []
                }