# -*- coding: utf-8 -*-
import BigWorld, math
from constants import ATTACK_REASONS, ARENA_GUI_TYPE
from account_helpers.settings_core.options import DamageLogDetailsSetting as _VIEW_MODE
from gui.battle_control.battle_constants import FEEDBACK_EVENT_ID as EV_ID, PERSONAL_EFFICIENCY_TYPE as _ETYPE
from gui.Scaleform.daapi.view.battle.shared.damage_log_panel import _LogViewComponent, DamageLogPanel as DLP
from PlayerEvents import g_playerEvents
from CurrentVehicle import g_currentVehicle
from shared_utils import BitmaskHelper
from functools import partial
from Core import macrosReplace, sessionProvider, g_itemsCache, healthDic, totalsDict, topLogDict
from Config import cfg
from BoEvents import eve
from BoConstants import DAMAGE_LOG

BASE_WG_LOGS = (DLP._updateTopLog, DLP._updateBottomLog, DLP._addToTopLog, DLP._addToBottomLog)
BASE_ADD_TO_LOG = []

class DamageLog(object):
    __slots__ = ('player', 'in_log', 'd_log', 'getVehicleInfo', 'isSPG', 'colors', 'guiType', 'isLegueStyle')
    def __init__(self):
        for attr in self.__slots__:
            setattr(self, attr, None)
        self.in_log = {}
        self.d_log = {}
        g_playerEvents.onPlayerEntityChanging += self.onPlayerEntityChanging
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle

    def wgLogFix(self, enable):
        if enable:
            DLP._updateBottomLog, DLP._updateTopLog, DLP._addToBottomLog, DLP._addToTopLog = BASE_WG_LOGS
        else:
            DLP._updateTopLog, DLP._updateBottomLog, DLP._addToTopLog, DLP._addToBottomLog = BASE_WG_LOGS

    def addToWGLog(self, b_self, events):
        if b_self._LogViewComponent__logViewMode == _VIEW_MODE.HIDE:
            return
        for e in events:
            eventType = e.getType()
            if eventType == _ETYPE.RECEIVED_CRITICAL_HITS and cfg.log_global['wg_log_hide_crits']:
                continue
            elif eventType == _ETYPE.BLOCKED_DAMAGE and cfg.log_global['wg_log_hide_block']:
                continue
            elif eventType in (_ETYPE.ASSIST_DAMAGE, _ETYPE.STUN) and cfg.log_global['wg_log_hide_assist']:
                continue
            elif BitmaskHelper.hasAnyBitSet(b_self._LogViewComponent__contentMask, eventType):
                vo = b_self._buildLogMessageVO(e)
                b_self._LogViewComponent__addToListProxy(**vo)

    def battleLoading(self, arenaDP, player, teams, isSPG):
        BASE_ADD_TO_LOG.append(_LogViewComponent.addToLog)
        if any([cfg.log_global['wg_log_hide_crits'], cfg.log_global['wg_log_hide_block'], cfg.log_global['wg_log_hide_assist']]):
            _LogViewComponent.addToLog = lambda b_self, events: self.addToWGLog(b_self, events)
        self.player = player
        self.isLegueStyle = cfg.hp_bars['style'] == "legue" and cfg.hp_bars["enabled"] or not cfg.hp_bars["enabled"]
        if cfg.log_damage_extended['enabled'] or cfg.log_input_extended['enabled']:
            eve.onKeyPressed += self.keyEvent
            eve.onVehicleAddUpdate += self.onVehicleAddUpdate
        sessionProvider.shared.feedback.onPlayerFeedbackReceived += self.onPlayerFeedbackReceived
        eve.onMainGunFailed += self.updateMainGun
        eve.onPlayerVehicleDeath += self.onPlayerVehicleDeath
        self.guiType = player.arena.guiType
        avgColor = cfg.colors['colorAvgDmg']
        self.colors = (avgColor['very_bad'], avgColor['bad'], avgColor['normal'], avgColor['good'], avgColor['very_good'], avgColor['unique'])
        self.getVehicleInfo = arenaDP.getVehicleInfo
        self.isSPG = isSPG
        if not totalsDict[DAMAGE_LOG.TANKAVGDAMAGE]:
            totalsDict[DAMAGE_LOG.TANKAVGDAMAGE] = max(1000, player.vehicle.typeDescriptor.maxHealth)
        topLogDict.update({}.fromkeys(DAMAGE_LOG.ETYPES, 0))
        if cfg.main_gun['enabled'] and healthDic[teams[1]][1]:
            totalsDict[DAMAGE_LOG.MAIN_GUN_INFO] = max(1000, int(math.ceil(healthDic[teams[1]][1] * 0.2)))
            self.updateMainGun()
        self.updateGlobalSumm()
        self.wgLogFix(cfg.log_global['wg_log_pos_fix'])

    def destroyBattle(self):
        if any([cfg.log_global['wg_log_hide_crits'], cfg.log_global['wg_log_hide_block'], cfg.log_global['wg_log_hide_assist']]) and BASE_ADD_TO_LOG:
            _LogViewComponent.addToLog = BASE_ADD_TO_LOG.pop(0)
        if cfg.log_damage_extended['enabled'] or cfg.log_input_extended['enabled']:
            eve.onKeyPressed -= self.keyEvent
            eve.onVehicleAddUpdate -= self.onVehicleAddUpdate
        sessionProvider.shared.feedback.onPlayerFeedbackReceived -= self.onPlayerFeedbackReceived
        eve.onMainGunFailed -= self.updateMainGun
        eve.onPlayerVehicleDeath -= self.onPlayerVehicleDeath
        self.in_log.clear()
        self.d_log.clear()
        topLogDict.clear()
        totalsDict.update({DAMAGE_LOG.MAIN_GUN_INFO:0, DAMAGE_LOG.GUN_DYNAMIC:0, DAMAGE_LOG.TANKAVGDAMAGE:0, DAMAGE_LOG.GUN_TEXT_LEGUE:"", DAMAGE_LOG.MG_CHECK:True, DAMAGE_LOG.MAXDMG:0})

    def keyEvent(self, key, isKeyDown):
        if key in cfg.log_global['logsAltmodeKey']:
            if cfg.log_damage_extended['enabled']:
                self.updateLog(self.d_log, cfg.log_damage_extended, "d_log", isKeyDown)
            if cfg.log_input_extended['enabled']:
                self.updateLog(self.in_log, cfg.log_input_extended, "in_log", isKeyDown)

    def getAvgColor(self):
        check = topLogDict[EV_ID.PLAYER_DAMAGED_HP_ENEMY]*100 / totalsDict[DAMAGE_LOG.TANKAVGDAMAGE]
        if check:
            return self.colors[[idx for idx, x in enumerate(DAMAGE_LOG.CONFINES) if check >= x][-1]]
        else:
            return self.colors[0]

    def onPlayerEntityChanging(self):
        intCD = getattr(g_currentVehicle.item, 'intCD', None)
        if intCD:
            avg = g_itemsCache.items.getVehicleDossier(intCD).getRandomStats().getAvgDamage()
            if avg is not None:
                totalsDict[DAMAGE_LOG.TANKAVGDAMAGE] = int(avg)

    def onPlayerFeedbackReceived(self, events):
        for event in events:
            eventType = event.getType()
            extra = event.getExtra()
            if eventType == EV_ID.PLAYER_SPOTTED_ENEMY:
                topLogDict[eventType] += event.getCount()
            elif eventType == EV_ID.PLAYER_DAMAGED_HP_ENEMY:
                damage = extra.getDamage()
                topLogDict[eventType] += damage
                if cfg.main_gun['mainGunDynamic']:
                    self.updateMainGun()
                if cfg.log_damage_extended['enabled']:
                    self.addToLog(self.d_log, cfg.log_damage_extended, "d_log", event.getTargetID(), extra.getAttackReasonID(), damage, extra.getShellType(), extra.isShellGold())
            elif eventType in DAMAGE_LOG.ETYPES[2:]:
                topLogDict[eventType] += extra.getDamage()
            elif cfg.log_input_extended['enabled'] and eventType == EV_ID.ENEMY_DAMAGED_HP_PLAYER:
                self.addToLog(self.in_log, cfg.log_input_extended, "in_log", event.getTargetID(), extra.getAttackReasonID(), extra.getDamage(), extra.getShellType(), extra.isShellGold())
            elif eventType == EV_ID.PLAYER_KILLED_ENEMY:
                BigWorld.callback(1, partial(self.killsUpdate, event.getTargetID()))
            if eventType in DAMAGE_LOG.ETYPES:
                self.updateGlobalSumm()

    def updateGlobalSumm(self):
        if cfg.log_total['enabled']:
            macro = {"{{damageIcon}}": cfg.log_total['damageIcon'],
             "{{blockedIcon}}": cfg.log_total['blockedIcon'],
             "{{assistIcon}}": cfg.log_total['assistIcon'],
             "{{spottedIcon}}": cfg.log_total['spottedIcon'],
             "{{playerDamage}}": "%d" % topLogDict[EV_ID.PLAYER_DAMAGED_HP_ENEMY],
             "{{blockedDamage}}": "%d" % topLogDict[EV_ID.PLAYER_USED_ARMOR],
             "{{assistDamage}}": "%d" % topLogDict[EV_ID.PLAYER_ASSIST_TO_KILL_ENEMY],
             "{{tankDamageAvgColor}}": self.getAvgColor(),
             "{{tankAvgDamage}}": "%d" % totalsDict[DAMAGE_LOG.TANKAVGDAMAGE],
             "{{spottedTanks}}": "%d" % topLogDict[EV_ID.PLAYER_SPOTTED_ENEMY],
             "{{stun}}": "",
             "{{stunIcon}}": "",
             "{{mainGun}}": ""}
            if self.isSPG:
                macro.update({"{{stun}}": "%d" % topLogDict[EV_ID.PLAYER_ASSIST_TO_STUN_ENEMY], "{{stunIcon}}": cfg.log_total['stunIcon']})
            if self.isLegueStyle:
                macro.update({"{{mainGun}}": "%s" % totalsDict[DAMAGE_LOG.GUN_TEXT_LEGUE]})
            eve.componentPy.as_updateDamageS(macrosReplace(cfg.log_total['templateMainDMG'], macro))

    def updateMainGun(self, dmg=False):
        if cfg.main_gun['enabled'] and self.guiType in (ARENA_GUI_TYPE.RANDOM, ARENA_GUI_TYPE.EPIC_RANDOM):
            dynamic = cfg.main_gun['mainGunDynamic']
            if dynamic:
                if totalsDict[DAMAGE_LOG.MAXDMG] and topLogDict[EV_ID.PLAYER_DAMAGED_HP_ENEMY] > totalsDict[DAMAGE_LOG.MAXDMG]:
                    totalsDict[DAMAGE_LOG.MAXDMG] = topLogDict[EV_ID.PLAYER_DAMAGED_HP_ENEMY]
                    totalsDict[DAMAGE_LOG.MG_CHECK] = True
                totalsDict[DAMAGE_LOG.GUN_DYNAMIC] = max(max(totalsDict[DAMAGE_LOG.MAIN_GUN_INFO], totalsDict[DAMAGE_LOG.MAXDMG]) - topLogDict[EV_ID.PLAYER_DAMAGED_HP_ENEMY], 0)
            mainGunAchived = dynamic and not totalsDict[DAMAGE_LOG.GUN_DYNAMIC] and totalsDict[DAMAGE_LOG.MG_CHECK]
            macro = {"{{mainGun}}": "" if mainGunAchived else "%d" % totalsDict[int(dynamic)],
             "{{mainGunIcon}}": cfg.main_gun['mainGunIcon'],
             "{{mainGunDoneIcon}}": cfg.main_gun['mainGunDoneIcon'] if mainGunAchived else "",
             "{{mainGunFailureIcon}}": cfg.main_gun['mainGunFailureIcon'] if not totalsDict[DAMAGE_LOG.MG_CHECK] and dynamic else "",
             "{{mainGunColor}}": cfg.colors['mainGun']['mainGunColor']}
            gun_text = macrosReplace(cfg.main_gun['template'], macro)
            if self.isLegueStyle:
                totalsDict[DAMAGE_LOG.GUN_TEXT_LEGUE] = gun_text
                if dmg:
                    self.updateGlobalSumm()
            else:
                eve.componentPy.as_mainGunTextS(gun_text)

    def killsUpdate(self, targetID):
        target = self.d_log.get(targetID)
        if target:
            target['kill'] = True
            self.updateLog(self.d_log, cfg.log_damage_extended, "d_log")

    def onPlayerVehicleDeath(self, killerID):
        killer = self.in_log.get(killerID)
        if killer:
            killer['kill'] = True
            self.updateLog(self.in_log, cfg.log_input_extended, "in_log")
        if cfg.main_gun['mainGunDynamic'] and totalsDict[DAMAGE_LOG.GUN_DYNAMIC]:
            totalsDict[DAMAGE_LOG.MG_CHECK] = False
            self.updateMainGun(True)

    def onVehicleAddUpdate(self, vehicleID, vehicleType):
        vehicle = self.in_log.get(vehicleID)
        if vehicle and vehicle['vehicleClass'] is None:
            vehicle['vehicleClass'] = vehicleType.classTag
            vehicle['name'] = vehicleType.shortName
            vehicle['iconName'] = vehicleType.iconName
            vehicle['lvl'] = vehicleType.level
            self.updateLog(self.in_log, cfg.log_input_extended, "in_log")

    def addToLog(self, logDict, settings, logName, vehicleID, attackReasonID, damage, shellType, gold):
        if shellType is None:
            shellType = self.player.getVehicleDescriptor().shot.shell.kind if attackReasonID == 0 and logName == "d_log" else "UNDEFINED"
        vehicle = logDict.setdefault(vehicleID, {})
        if vehicle:
            vehicle['totalDamage'] += damage
            vehicle['lastDamage'] = damage
            vehicle['shots'] += int(not attackReasonID)
            vehicle['attackReason'] = ATTACK_REASONS[attackReasonID]
            vehicle['shellType'] = settings['shellTypes'][shellType]
            vehicle['shellColor'] = settings['shellColor'][DAMAGE_LOG.SHELL[int(gold)]]
        else:
            info = self.getVehicleInfo(vehicleID)
            vehicle['index'] = len(logDict)
            vehicle['totalDamage'] = damage
            vehicle['lastDamage'] = damage
            vehicle['shots'] = int(not attackReasonID)
            vehicle['attackReason'] = ATTACK_REASONS[attackReasonID]
            vehicle['shellType'] = settings['shellTypes'][shellType]
            vehicle['shellColor'] = settings['shellColor'][DAMAGE_LOG.SHELL[int(gold)]]
            vehicle['vehicleClass'] = info.vehicleType.classTag
            vehicle['name'] = info.vehicleType.shortName
            vehicle['iconName'] = info.vehicleType.iconName
            vehicle['userName'] = info.player.name
            vehicle['lvl'] = info.vehicleType.level
            vehicle['kill'] = False
            if logName == "in_log":
                isEnemy = info.team != self.player.team
                vehicle['dmgColor'] = 'isEnemy' if isEnemy else 'isSquadMan' if sessionProvider.getCtx().isSquadMan(vehicleID) else 'isTeamDamage'
        self.updateLog(logDict, settings, logName)

    def updateLog(self, logDict, settings, logName, logaltmode=False):
        log = []
        template = settings['extendedLog'] if not logaltmode else settings['extendedLogALTMODE']
        vType = cfg.vehicle_types
        classIcon = vType['vehicleClassIcon']
        classColor = vType['vehicleClassColors']
        for key in sorted(logDict, key=lambda val: logDict[val]['index'], reverse=settings['reverse']):
            log_info = logDict[key]
            vehicleClass = log_info['vehicleClass']
            macro = {"{{index}}": "%02d" % log_info['index'],
             "{{shots}}": "%d" % log_info['shots'],
             "{{totalDamage}}": "%d" % log_info['totalDamage'],
             "{{lastDamage}}": "%d" % log_info['lastDamage'],
             "{{classIcon}}": classIcon.get(vehicleClass, classIcon["unknown"]),
             "{{tankClassColor}}": classColor.get(vehicleClass, classColor["unknown"]),
             "{{iconName}}": log_info['iconName'],
             "{{tankName}}": log_info['name'],
             "{{userName}}": "%.12s" % log_info['userName'],
             "{{TankLevel}}": "%s" % log_info['lvl'],
             "{{shellType}}": "%s" % log_info['shellType'],
             "{{shellColor}}": "%s" % log_info['shellColor'],
             "{{attackReason}}": cfg.log_global['attackReason'][log_info['attackReason']],
             "{{killedIcon}}": settings['killedIcon'] if log_info['kill'] else ""}
            if logName == "in_log":
                macro.update({"{{attackerColor}}": cfg.colors['attackerColor'][log_info['dmgColor']]})
            log.append(macrosReplace(template, macro))
        eve.componentPy.as_updateLogS(logName, '%s%s%s' % (settings['startTag'], '<br>'.join(log), settings['endTag']))