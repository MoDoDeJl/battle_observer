# -*- coding: utf-8 -*-
import Math
from Core import sessionProvider, macrosReplace
from Config import cfg
from BoEvents import eve
from gui.Scaleform.daapi.view.meta.CrosshairPanelContainerMeta import CrosshairPanelContainerMeta

VECTOR = Math.Vector3(0.0, 0.0, 0.0)
BASE_DISTANCE = CrosshairPanelContainerMeta.as_setDistanceS

class FlightTime(object):
    __slots__ = ('player', 'template', 'isSPG', 'enable', 'isSpgOnly')
    def __init__(self):
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle
        for attr in self.__slots__:
            setattr(self, attr, None)

    def battleLoading(self, arenaDP, player, teams, isSPG):
        self.enable = cfg.flight_time['enabled']
        self.isSpgOnly = cfg.flight_time['spgOnly']
        self.player = player
        self.isSPG = isSPG
        if self.enable:
            if self.isSpgOnly and not isSPG:
                return
            self.template = cfg.flight_time['template']
            sessionProvider.shared.crosshair.onGunMarkerStateChanged += self.__onGunMarkerStateChanged
            player.arena.onVehicleKilled += self.__onVehicleKilled
            if '{{distance}}' in self.template:
                CrosshairPanelContainerMeta.as_setDistanceS = lambda *args: None

    def destroyBattle(self):
        if self.enable:
            if self.isSpgOnly and not self.isSPG:
                return
            sessionProvider.shared.crosshair.onGunMarkerStateChanged -= self.__onGunMarkerStateChanged
            self.player.arena.onVehicleKilled -= self.__onVehicleKilled
            if '{{distance}}' in self.template:
                CrosshairPanelContainerMeta.as_setDistanceS = BASE_DISTANCE

    def __onVehicleKilled(self, targetID, attackerID, equipmentID, reason):
        if targetID == self.player.playerVehicleID:
            eve.componentPy.as_flyghtTimeS('')

    def __onGunMarkerStateChanged(self, markerType, position, params, collision):
        shotPos, shotVec = self.player.gunRotator.getCurShotPosition()
        flatDist = position.flatDistTo(shotPos)
        eve.componentPy.as_flyghtTimeS(macrosReplace(self.template, {"{{flightTime}}": "%.2f" % (flatDist / shotVec.flatDistTo(VECTOR)), "{{distance}}": "%.1f" % flatDist}))