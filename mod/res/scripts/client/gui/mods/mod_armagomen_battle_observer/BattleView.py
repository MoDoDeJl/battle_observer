# -*- coding: utf-8 -*-
import BigWorld
from constants import ARENA_GUI_TYPE
from gui.shared import events, g_eventBus, EVENT_BUS_SCOPE
from gui.Scaleform.framework.entities.View import View
from gui.Scaleform.framework import g_entitiesFactories, ViewSettings, ViewTypes, ScopeTemplates
from gui.Scaleform.framework.managers.loaders import ViewLoadParams
from gui.app_loader.loader import g_appLoader
from gui.app_loader.settings import APP_NAME_SPACE
from gui.Scaleform.genConsts.BATTLE_VIEW_ALIASES import BATTLE_VIEW_ALIASES as BVA
from Config import cfg
from BoEvents import eve
from Core import g_settingsCore
from account_helpers.settings_core.settings_constants import GRAPHICS

RANDOM_TYPE = (ARENA_GUI_TYPE.RANDOM, ARENA_GUI_TYPE.EPIC_RANDOM)
RANDOM_PANEL = (BVA.FRAG_CORRELATION_BAR, BVA.EPIC_RANDOM_SCORE_PANEL)
ALIAS = 'ModBattleObserverUI'
    
class ModBattleObserverMeta(View):

    def _populate(self):
        super(ModBattleObserverMeta, self)._populate()
        if hasattr(self.flashObject, "as_startUpdate"):
            self.__startUpdate()
            eve.startFlash(self)

    def _dispose(self):
        eve.componentPy = None
        super(ModBattleObserverMeta, self)._dispose()
        
    def flashLogS(self, *logInfo):
        print '[NOTE] {}'.format(','.join(str(x) for x in logInfo))

    def __startUpdate(self):
        style = cfg.hp_bars['style']
        data = {
            'bg': {
                'bg_vis': cfg.main['background'] and style == "normal",
                'bg_alpha': cfg.main['backgroundTransparency'],
                'user_bg': cfg.main['user_background']},
            'style': style,
            'bars': {
                'enabled': cfg.hp_bars["enabled"],
                'width': max(100, cfg.hp_bars['barsWidth']),
                'bgHpColor': int(cfg.colors['hpBar']['backgroundHpBarColor'].replace('#', ''), 16),
                'hpBarsAlpha': cfg.colors['hpBar']['hpBarsTransparency'],
                'alpha': cfg.colors['hpBar']['bg_bars_alpha']},
            'colors': {
                'ally': int(cfg.colors['hpBar']['alliesHpBarColor'].replace('#', ''), 16),
                'enemy': int(cfg.colors['hpBar']['enemyHpBarColor%s' % ('Blind' if g_settingsCore.getSetting(GRAPHICS.COLOR_BLIND) else '')].replace('#', ''), 16)},
            'bases': {
                'y': cfg.team_bases_panel['y'],
                'scale': cfg.team_bases_panel['scale']},
            'markers': {
                'ax': cfg.markers['position']['ally_x'][style],
                'ay': cfg.markers['position']['ally_y'][style],
                'ex': cfg.markers['position']['enemy_x'][style],
                'ey': cfg.markers['position']['enemy_y'][style]},
            'logsPos': {
                'dx': cfg.log_damage_extended['damageLogPosition']['x'],
                'dy': cfg.log_damage_extended['damageLogPosition']['y'],
                'ix': cfg.log_input_extended['inputLogPosition']['x'],
                'iy': cfg.log_input_extended['inputLogPosition']['y'],
                'tx': cfg.log_total['logPosition_x'],
                'ty': cfg.log_total['logPosition_y'],
                'mgx': cfg.main_gun['position_x'],
                'mgy': cfg.main_gun['position_y'],
                'scale': cfg.log_total['mainLogScale']},
            'calc': {
                'cx': cfg.armor_calculator['calcPosition']['x'],
                'cy': cfg.armor_calculator['calcPosition']['y'],
                'tx': cfg.armor_calculator['calcTextPosition']['x'],
                'ty': cfg.armor_calculator['calcTextPosition']['y'],
                'fx': cfg.flight_time['pos_x'],
                'fy': cfg.flight_time['pos_y']},
            'debug': {
                'x': cfg.debugPanel['position_x'],
                'y': cfg.debugPanel['position_y'],
                'scale': cfg.debugPanel['scale']}
            }
        self.flashObject.as_startUpdate(data)
    
    def as_AddVehIdToListS(self, vehID):
        return self.flashObject.as_AddVehIdToList(vehID) if self._isDAAPIInited() else None
    
    def as_AddPPanelBarS(self, vehID, color, x, y, width, height, teamName):
        return self.flashObject.as_AddPPanelBar(vehID, color, x, y, width, height, teamName) if self._isDAAPIInited() else None
    
    def as_AddTextFieldS(self, vehID, name, x, y, align, teamName):
        return self.flashObject.as_AddTextField(vehID, name, x, y, align, teamName) if self._isDAAPIInited() else None
    
    def as_updateTextFieldS(self, vehicleID, name, text):
        return self.flashObject.as_updateTextField(vehicleID, name, text) if self._isDAAPIInited() else None
    
    def as_updatePPanelBarS(self, vehicleID, currHP, maxHP, textField):
        return self.flashObject.as_updatePPanelBar(vehicleID, currHP, maxHP, textField) if self._isDAAPIInited() else None
    
    def as_setVehicleClassColorS(self, vehID, color):
        return self.flashObject.as_setVehicleClassColor(vehID, color) if self._isDAAPIInited() else None
        
    def as_setHPbarsVisibleS(self, vehID, visible):
        return self.flashObject.as_setHPbarsVisible(vehID, visible) if self._isDAAPIInited() else None
        
    def as_setPlayersDamageVisibleS(self, vehID, visible):
        return self.flashObject.as_setPlayersDamageVisible(vehID, visible) if self._isDAAPIInited() else None
    
    def as_flyghtTimeS(self, text):
        return self.flashObject.as_flyghtTime(text) if self._isDAAPIInited() else None

    def as_armorCalcS(self, text):
        return self.flashObject.as_armorCalc(text) if self._isDAAPIInited() else None

    def as_armorTextSS(self, text):
        return self.flashObject.as_armorTextS(text) if self._isDAAPIInited() else None

    def as_updateLogS(self, inLog, text):
        return self.flashObject.as_updateLog(inLog, text) if self._isDAAPIInited() else None

    def as_updateDamageS(self, text):
        return self.flashObject.as_updateDamage(text) if self._isDAAPIInited() else None

    def as_mainGunTextS(self, text):
        return self.flashObject.as_mainGunText(text) if self._isDAAPIInited() else None

    def up_fpsPingS(self, debug):
        return self.flashObject.up_fpsPing(debug) if self._isDAAPIInited() else None

    def as_UpdateTimeS(self, clock, timer):
        return self.flashObject.as_UpdateTime(clock, timer) if self._isDAAPIInited() else None

    def as_updateScoreS(self, ally, enemy):
        return self.flashObject.as_updateScore(ally, enemy) if self._isDAAPIInited() else None

    def as_markersS(self, ally, enemy):
        return self.flashObject.up_markers(ally, enemy) if self._isDAAPIInited() else None

    def as_updateBasesS(self, param, progressBar, invadersCnt, time):
        return self.flashObject.as_updateBases(param, progressBar, invadersCnt, time) if self._isDAAPIInited() else None

    def as_updateBaseTextS(self, param, text):
        return self.flashObject.as_updateBaseText(param, text) if self._isDAAPIInited() else None

    def as_setBaseVisibleS(self, base, visible):
        return self.flashObject.as_setBaseVisible(base, visible) if self._isDAAPIInited() else None
        
    def as_updateHealthS(self, team, bar, healths):
        return self.flashObject.as_updateHealth(team, bar, healths) if self._isDAAPIInited() else None

    def as_differenceS(self, diff):
        return self.flashObject.as_difference(diff) if self._isDAAPIInited() else None

    def as_colorBlindS(self, enemy_color):
        return self.flashObject.as_colorBlind(enemy_color) if self._isDAAPIInited() else None
        
    def as_colorBlindPPbarsS(self, vehicleID, color):
        return self.flashObject.as_colorBlindPPbars(vehicleID, color) if self._isDAAPIInited() else None
        
    def as_MinimapCenteredS(self, enable):
        return self.flashObject.as_MinimapCentered(enable) if self._isDAAPIInited() else None

def onComponentRegistered(event):
    alias = event.alias
    if alias == BVA.BATTLE_STATISTIC_DATA_CONTROLLER:
        app = g_appLoader.getDefBattleApp()
        if app:
            app.loadView(ViewLoadParams(ALIAS, ALIAS), {})
    elif alias in RANDOM_PANEL and cfg.hp_bars["enabled"]:
        event.componentPy.flashObject.visible = False
    elif alias == BVA.DEBUG_PANEL and cfg.debugPanel['enabled']:
        event.componentPy.flashObject.alpha = 0.0
        event.componentPy.flashObject.visible = False
    elif alias == BVA.BATTLE_TIMER and cfg.battleTimer['enabled']:
        event.componentPy.flashObject.alpha = 0.0
        event.componentPy.flashObject.visible = False
    elif alias == BVA.BATTLE_MESSENGER and BigWorld.player().arena.guiType in RANDOM_TYPE and cfg.main['hideChatInRandom']:
        event.componentPy.flashObject.visible = False

g_entitiesFactories.addSettings(ViewSettings(ALIAS, ModBattleObserverMeta, 'modBattleObserver.swf', ViewTypes.WINDOW, None, ScopeTemplates.DEFAULT_SCOPE, False))
g_eventBus.addListener(events.ComponentEvent.COMPONENT_REGISTERED, onComponentRegistered, scope=EVENT_BUS_SCOPE.GLOBAL)