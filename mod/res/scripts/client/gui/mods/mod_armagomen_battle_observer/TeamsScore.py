# -*- coding: utf-8 -*-
import BigWorld
from constants import ARENA_GUI_TYPE
from Config import cfg
from Core import sessionProvider
from BoEvents import eve

class TeamsScore(object):
    __slots__ = ('isAlly', 'score', 'showAliveCount')
    def __init__(self):
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle
        for attr in self.__slots__:
            setattr(self, attr, None)

    def battleLoading(self, arenaDP, player, teams, isSPG):
        if cfg.hp_bars["enabled"]:
            self.isAlly = sessionProvider.getCtx().isAlly
            self.showAliveCount = player.arena.guiType != ARENA_GUI_TYPE.UNKNOWN and cfg.hp_bars['showAliveCount']
            self.score = [arenaDP.getAlliesVehiclesNumber(), arenaDP.getEnemiesVehiclesNumber()] if self.showAliveCount else [0, 0]
            eve.componentPy.as_updateScoreS(*self.score)
            player.arena.onVehicleKilled += self.__updateScore

    def destroyBattle(self):
        if cfg.hp_bars["enabled"]:
            self.score = [0, 0]
            BigWorld.player().arena.onVehicleKilled -= self.__updateScore

    def __updateScore(self, targetID, attackerID, equipmentID, reason):
        if self.showAliveCount:
            self.score[int(not self.isAlly(targetID))] -= 1
        else:
            self.score[int(self.isAlly(targetID))] += 1
        eve.componentPy.as_updateScoreS(*self.score)