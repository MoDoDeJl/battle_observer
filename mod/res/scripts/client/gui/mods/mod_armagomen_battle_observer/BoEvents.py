# -*- coding: utf-8 -*-
import Event, BigWorld
from gui import InputHandler
from Vehicle import Vehicle
from BoConstants import EPIC_BATTLE
from gui.shared import events, g_eventBus, EVENT_BUS_SCOPE
from gui.Scaleform.daapi.view.battle.shared.page import SharedPage
from gui.battle_control.arena_info import vos_collections
from Core import sessionProvider, healthDic, g_settingsCore, vehiclesDic
from account_helpers.settings_core.settings_constants import GRAPHICS
from gui.Scaleform.daapi.view.meta.BattlePageMeta import BattlePageMeta
from gui.Scaleform.genConsts.BATTLE_VIEW_ALIASES import BATTLE_VIEW_ALIASES
from Config import cfg

class BoEvents(object):
    __slots__ = ('player', 'componentPy', 'battleLoading', 'destroyBattle', 'onHealthChanged', 'onPlayersDamaged', 'onVehicleAddUpdate', 'onMainGunFailed', 'onPlayerVehicleDeath', 'drawGraphics', 'onKeyPressed', 'pressedKeys')
    def __init__(self):
        self.player = None
        self.componentPy = None
        self.battleLoading = Event.Event()
        self.destroyBattle = Event.Event()
        self.onHealthChanged = Event.Event()
        self.onPlayersDamaged = Event.Event()
        self.onVehicleAddUpdate = Event.Event()
        self.onMainGunFailed = Event.Event()
        self.onPlayerVehicleDeath = Event.Event()
        self.drawGraphics = Event.Event()
        self.onKeyPressed = Event.Event()
        self.__hooks()

    def __hooks(self):
        base_reload = SharedPage.reload
        base_stopBattle = SharedPage._stopBattleSession
        base_visible = BattlePageMeta.as_setComponentsVisibilityS
        base_health = Vehicle.onHealthChanged
        SharedPage.reload = lambda base_self: self.reload(base_self, base_reload)
        SharedPage._stopBattleSession = lambda base_self: self._stopBattle(base_self, base_stopBattle)
        BattlePageMeta.as_setComponentsVisibilityS = lambda base_self, visible, hidden: self.setComponentsVisibilityS(base_self, base_visible, visible, hidden)
        Vehicle.onHealthChanged = lambda vehicle, newHealth, attackerID, aRID: self.healthChanged(vehicle, base_health, newHealth, attackerID, aRID)
        g_eventBus.addListener(events.GameEvent.BATTLE_LOADING, self.battleLoadingFinish, EVENT_BUS_SCOPE.BATTLE)

    def keyEvent(self, event):
        key = event.key
        if key in self.pressedKeys:
            if not self.player.isForcedGuiControlMode() or key in (29, 157):
                self.onKeyPressed(key, event.isKeyDown())

    def setComponentsVisibilityS(self, base_self, base, visible, hidden):
        base(base_self, visible, hidden)
        if self.componentPy:
            self.componentPy.flashObject.visible = BATTLE_VIEW_ALIASES.FULL_STATS not in visible

    def reload(self, base_self, base):
        if self.componentPy:
            self.componentPy.flashObject.visible = False
            self.componentPy.as_updateLogS("d_log", "")
            self.componentPy.as_updateLogS("in_log", "")
        self.stopBattle()
        base(base_self)

    def battleLoadingFinish(self, event):
        if not event.ctx['isShown']:
            BigWorld.callback(0.2, self.loading)

    def _stopBattle(self, base_self, base):
        base(base_self)
        self.stopBattle()

    def startFlash(self, componentPy):
        if componentPy and self.componentPy is None:
            self.componentPy = componentPy
            componentPy.flashObject.visible = False

    def loading(self):
        self.player = BigWorld.player()
        self.pressedKeys = cfg.markers['showMarkers_KEY'] + cfg.log_global['logsAltmodeKey'] + cfg.players_damages['damages_key'] + cfg.players_bars['hpbarsShowKey'] + cfg.minimap['key']
        if self.componentPy and self.player:
            arenaDP = sessionProvider.getArenaDP()
            teams = (arenaDP.getNumberOfTeam(), arenaDP.getNumberOfTeam(enemy=True))
            for team in teams:
                healthDic.setdefault(team, [0, 0])
            for vInfoVO in vos_collections.VehiclesInfoCollection().iterator(arenaDP):
                maxHealth = vInfoVO.vehicleType.maxHealth
                curHealth = maxHealth if vInfoVO.isAlive() else 0
                team = vInfoVO.team
                healthDic[team][0] += curHealth
                healthDic[team][1] += maxHealth
                vehiclesDic[vInfoVO.vehicleID] = [curHealth, maxHealth, team, vInfoVO.vehicleType.classTag]
                if self.player.arena.bonusType not in EPIC_BATTLE:
                    self.componentPy.as_AddVehIdToListS(vInfoVO.vehicleID)
            self.battleLoading(arenaDP, self.player, teams, 'SPG' in self.player.vehicleTypeDescriptor.type.tags)
            InputHandler.g_instance.onKeyDown += self.keyEvent
            InputHandler.g_instance.onKeyUp += self.keyEvent
        else:
            BigWorld.callback(1, self.loading)

    def stopBattle(self):
        healthDic.clear()
        vehiclesDic.clear()
        if self.componentPy:
            InputHandler.g_instance.onKeyDown -= self.keyEvent
            InputHandler.g_instance.onKeyUp -= self.keyEvent
            self.destroyBattle()
            self.player = None

    def healthChanged(self, vehicle, baseFunc, newHealth, attackerID, aRID):
        baseFunc(vehicle, newHealth, attackerID, aRID)
        self.onHealthChanged(vehicle.id, vehicle.publicInfo.team, max(0, newHealth), attackerID, aRID)

eve = BoEvents()