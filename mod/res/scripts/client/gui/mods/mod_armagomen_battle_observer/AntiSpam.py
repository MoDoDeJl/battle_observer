# -*- coding: utf-8 -*-
import debug_utils
from Config import cfg

class AntiSpam(object):
    __slots__ = ()
    def __init__(self):
        base_dolog = debug_utils._doLog
        debug_utils._doLog = lambda category, msg, *args, **kwargs: self._doLog(base_dolog, category, msg, *args, **kwargs)
        debug_utils._makeMsgHeader = lambda frame: '[FILTERED]'

    def _doLog(srlf, base_dolog, category, msg, *args, **kwargs):
        if category in ('WARNING', 'DEBUG', 'GUI') or 'download' in msg.lower() or 'player' in msg.lower() or 'HANGAR' in msg or 'quest' in msg:
            return
        return base_dolog(category, msg, *args, **kwargs)

if cfg.main['PYTHON_LOG_ANTISPAM']:
    AntiSpam()
    print "[BATTLE_OBSERVER_INFO] PYTHON LOG FILTER IS ENABLED"