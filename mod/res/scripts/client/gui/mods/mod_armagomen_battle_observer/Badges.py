# -*- coding: utf-8 -*-
from gui.battle_control.arena_info import vos_collections
from gui.Scaleform.daapi.view.battle.shared.stats_exchage.stats_ctrl import BattleStatisticsDataController as BSDC
from Config import cfg
NAMES = {6698174:'Армагомен', 25106393:'ШураББ'}
BASE_INVALIDATE = BSDC.invalidateVehiclesInfo
BASE_UPDATE = BSDC.updateVehiclesInfo
BASE_ADD = BSDC.addVehicleInfo

class Badges(object):
    __slots__ = ()
    def __init__(self):
        BSDC.invalidateVehiclesInfo = lambda base_self, arenaDP: self.invalidateVInfo(base_self, arenaDP)
        BSDC.updateVehiclesInfo = lambda base_self, updated, arenaDP: self.updateVInfo(base_self, updated, arenaDP)
        BSDC.addVehicleInfo = lambda base_self, vInfoVO, arenaDP: self.addVInfo(base_self, vInfoVO, arenaDP)

    def addVInfo(self, base_self, vInfoVO, arenaDP):
        vInfoVO = self.setBadgesAndName(vInfoVO.player.accountDBID, vInfoVO, True)
        BASE_ADD(base_self, vInfoVO, arenaDP)

    def updateVInfo(self, base_self, updated, arenaDP):
        for flags, vInfoVO in updated:
            self.setBadgesAndName(vInfoVO.player.accountDBID, vInfoVO)
        BASE_UPDATE(base_self, updated, arenaDP)

    def invalidateVInfo(self, base_self, arenaDP):
        for vInfoVO in vos_collections.VehiclesInfoCollection().iterator(arenaDP):
            self.setBadgesAndName(vInfoVO.player.accountDBID, vInfoVO)
        BASE_INVALIDATE(base_self, arenaDP)

    def setBadgesAndName(self, accountDBID, vInfoVO, add=False):
        if cfg.main['hideBadges']:
            vInfoVO.ranked.badges = ()
        if accountDBID in NAMES:
            vInfoVO.player.name = NAMES[accountDBID]
        if add:
            return vInfoVO