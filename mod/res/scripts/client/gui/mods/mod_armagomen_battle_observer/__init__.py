# -*- coding: utf-8 -*-
import traceback
import codecs
import time
import BigWorld
import ResMgr

def setClientVersion():
    for sec in ResMgr.openSection('../paths.xml')['Paths'].values():
        dirPath = sec.asString
        if './mods/' in dirPath:
            BigWorld.currentModPathBO = dirPath

if not hasattr(BigWorld, 'mod_battle_observer'):
    try:
        setClientVersion()
        import Core, Config, guiSettings, BoEvents, BattleView, ArmorCalculator, TeamsHP, PlayersPanels, Timer, TeamsScore, TeamBases, Markers
        import Debug, Damagelog, PlDamage, FlightTime, Camera, Badges, AntiSpam, Analytics, SaveShoot, Update, BOMinimap
        guiSettings.ConfigInterface()
        ArmorCalculator.ArmorCalculator()
        TeamsHP.TeamsHP()
        PlayersPanels.PlayersPanels()
        Timer.Timer()
        TeamsScore.TeamsScore()
        TeamBases.TeamBases()
        Markers.Markers()
        Debug.Debug()
        Damagelog.DamageLog()
        PlDamage.PlDamage()
        FlightTime.FlightTime()
        Camera.Camera()
        Badges.Badges()
        Analytics.Analytics()
        SaveShoot.SaveShootLite()
        BOMinimap.BOMinimap()
        BigWorld.mod_battle_observer = True
    except Exception:
        path = './mods/configs/mod_battle_observer/Errors.log'
        print 'BATTLE OBSERVER LOAD ERROR - MOD SHUTTING DOWN, WATCH DIR - ' + path
        with codecs.open(path, 'a', 'utf-8-sig') as fh:
            fh.write("=====TRACEBACK==START===== (%s)\n" % time.asctime())
            traceback.print_exc(file=fh)
            fh.write("=====TRACEBACK==FINISH=====\n\n")
   
def fini():
    if hasattr(BigWorld, 'mod_battle_observer'):
        try:
            Core.fini()
        except Exception:
            path = './mods/configs/mod_battle_observer/Errors.log'
            with codecs.open(path, 'a', 'utf-8-sig') as fh:
                fh.write("=====TRACEBACK==START===== (%s)\n" % time.asctime())
                traceback.print_exc(file=fh)
                fh.write("=====TRACEBACK==FINISH=====\n\n")