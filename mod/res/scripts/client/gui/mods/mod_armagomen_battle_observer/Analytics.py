# -*- coding: utf-8 -*-
import threading
import urllib
import urllib2
from constants import AUTH_REALM
from Core import mod_name, mod_version, DEFAULT_LANGUAGE, connectionManager
from Config import c_Loader

class Analytics(object):
    __slots__ = ('user', )
    def __init__(self):
        self.user = None
        connectionManager.onLoggedOn += self.start
        connectionManager.onDisconnected += self.end

    def start(self, data):
        if self.user is None and 'token2' in data:
            self.user = data['token2'].split(':')[0]
            threading.Thread(target=self.trySendStat('start', self.user, 'screenview')).start()

    def end(self):
        if self.user is not None:
            threading.Thread(target=self.trySendStat('end', self.user, 'event')).start()
            self.user = None

    def trySendStat(self, param, user, event):
        paramas = {'sc':param,'v':1,'tid':'UA-81349715-2','cid':user,'t':event,'an':mod_name,'av':mod_version,'aid':c_Loader.cName,'cd':'Cluster: [%s-%s]' % (AUTH_REALM, DEFAULT_LANGUAGE.upper()),'ul':DEFAULT_LANGUAGE.upper()}
        if event == 'event':
            paramas.update({'ec':event, 'ea':'disconnect'})
        data = urllib.urlencode(paramas)
        try:
            req = urllib2.Request('http://www.google-analytics.com/collect', data, {'User-Agent': '%s/%s' % (mod_name, mod_version)})
            response = urllib2.urlopen(req, timeout=3)
            response.read()
            response.close()
        except Exception as error:
            print 'Unable to send data analytics: %s' % error