# -*- coding: utf-8 -*-
import BigWorld
from Core import playersDamage, totalsDict, topLogDict, vehiclesDic, sessionProvider
from BoEvents import eve
from Config import cfg
from BoConstants import DAMAGE_LOG, EPIC_BATTLE
from constants import ATTACK_REASON_INDICES

FIELD = "DamageTf"

class PlDamage(object):
    __slots__ = ('playerID', 'damage_on_alt', 'damage_temaplate', 'isEnemy')
    def __init__(self):
        eve.battleLoading += self.battleLoading
        eve.destroyBattle += self.destroyBattle
        self.playerID = 0

    def battleLoading(self, arenaDP, player, teams, isSPG):
        if player.arena.bonusType not in EPIC_BATTLE:
            self.damage_on_alt = cfg.players_damages['enabled']
            self.damage_temaplate = cfg.players_damages['damages_text']
            self.playerID = player.playerVehicleID
            self.isEnemy = sessionProvider.getCtx().isEnemy
            eve.onPlayersDamaged += self.onPlayersDamaged
            eve.onKeyPressed += self.keyEvent
            eve.onVehicleAddUpdate += self.onVehicleAdd
            if self.damage_on_alt:
                for vehicleID in vehiclesDic:
                    eve.componentPy.as_AddTextFieldS(vehicleID, FIELD, cfg.players_damages["damages_settings"]["x"], cfg.players_damages["damages_settings"]["y"], cfg.players_damages["damages_settings"]["align"], "red" if self.isEnemy(vehicleID) else "green")

    def destroyBattle(self):
        if BigWorld.player().arena.bonusType not in EPIC_BATTLE:
            eve.onPlayersDamaged -= self.onPlayersDamaged
            eve.onKeyPressed -= self.keyEvent
            eve.onVehicleAddUpdate -= self.onVehicleAdd
            playersDamage.clear()
        
    def onVehicleAdd(self, vehicleID, vehicleType):
        eve.componentPy.as_AddTextFieldS(vehicleID, FIELD, cfg.players_damages["damages_settings"]["x"], cfg.players_damages["damages_settings"]["y"], cfg.players_damages["damages_settings"]["align"], "red" if self.isEnemy(vehicleID) else "green")
        
    def keyEvent(self, key, isKeyDown):
        if self.damage_on_alt and key in cfg.players_damages['damages_key']:
            for vehicleID in vehiclesDic:
                eve.componentPy.as_setPlayersDamageVisibleS(vehicleID, isKeyDown)

    def onPlayersDamaged(self, attackerID, damage, targetTeam, attakerTeam, aRID):
        p_damage = playersDamage.setdefault(attackerID, [0, False])
        if targetTeam == attakerTeam:
            p_damage[1] |= aRID == ATTACK_REASON_INDICES['shot']
        else:
            p_damage[0] += damage
            if self.damage_on_alt:
                eve.componentPy.as_updateTextFieldS(attackerID, FIELD, self.damage_temaplate.replace("{{damage}}", "%d" % p_damage[0]))
        if cfg.main_gun['enabled'] and cfg.main_gun['mainGunDynamic']:
            if attackerID != self.playerID:
                if p_damage[0] > totalsDict[DAMAGE_LOG.MAXDMG] and not p_damage[1]:
                    if p_damage[0] > max(totalsDict[DAMAGE_LOG.MAIN_GUN_INFO], topLogDict[DAMAGE_LOG.ETYPES[1]]):
                        totalsDict[DAMAGE_LOG.MAXDMG] = p_damage[0]
                        self.setMGfail()
            else:
                if p_damage[1]:
                    self.setMGfail()

    def setMGfail(self):
        totalsDict[DAMAGE_LOG.MG_CHECK] = False
        eve.onMainGunFailed(True)