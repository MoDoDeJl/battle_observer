@Echo off
set SouceDir=D:\Develop\Battle_Observer\battle_observer\mod\
set ModSouce=%SouceDir%res\scripts\client\gui\mods\
set ModVer=1.15.3

set WotVer="1.0.1.1"
REM set WotVer="1.0.1 Common Test"
set ModsDir=C:\Games\World_of_Tanks\mods\
REM set ModsDir=D:\Games\World_of_Tanks_CT\mods\

"D:\Program\PjOrion\PjOrion.exe" /compile-folder "%ModSouce%" /exit
REM "D:\Program\PjOrion\PjOrion.exe" /protect-bytecode-folder "%ModSouce%" /exit

set ModFile=%ModsDir%%WotVer%\armagomen.battleObserver_%ModVer%.wotmod
set ZipArh=D:\Develop\Battle_Observer\BO_%ModVer%_WOT_%WotVer%.zip

DEL %ModFile%
DEL %ZipArh%

"%ProgramFiles%\7-Zip\7z.exe" a -tzip -r -mx0 -x!*.py -ssw %ModFile% %SouceDir%*
"%ProgramFiles%\7-Zip\7z.exe" a -tzip -r -mx9 -x!*.txt -x!*.db -x!*.log -x!vxSettingsApi -ssw %ZipArh% %ModsDir%

set lastUpdate=D:\Develop\Battle_Observer\BattleObserver_LastUpdate.zip
DEL %lastUpdate%
"%ProgramFiles%\7-Zip\7z.exe" a -tzip -r -mx9 -x!*.txt -x!*.db -x!configs -ssw %lastUpdate% %ModFile%